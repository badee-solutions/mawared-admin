<?php

/*
 * Copyright (c) 2017 Salah Alkhwlani <yemenifree@yandex.com>
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Modules\Admin\Forms\Fields;

use Kris\LaravelFormBuilder\Fields\FormField;

class Collapse extends FormField
{
    protected $template_name = 'admin::form.start-collapse';

    public function render(array $options = [], $showLabel = true, $showField = true, $showError = true)
    {
        if ($this->getOption('type', false) === true || $this->getOption('type_group', false) === true) {
            $this->template = 'admin::form.end-collapse';
        }

        return parent::render($options, $showLabel, $showField, $showError);
    }

    protected function getTemplate()
    {
        return $this->template_name;
    }
}
