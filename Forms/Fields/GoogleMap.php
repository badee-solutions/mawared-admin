<?php

/*
 * Copyright (c) 2017 Salah Alkhwlani <yemenifree@yandex.com>
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Modules\Admin\Forms\Fields;

use Kris\LaravelFormBuilder\Fields\FormField;

class GoogleMap extends FormField
{
    protected function getTemplate()
    {
        return 'admin::form.map';
    }

    public function render(array $options = [], $showLabel = true, $showField = true, $showError = true)
    {
        return parent::render($options, $showLabel, $showField, $showError);
    }
}
