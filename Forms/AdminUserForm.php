<?php

/*
 * Copyright (c) 2017 Salah Alkhwlani <yemenifree@yandex.com>
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Modules\Admin\Forms;

use Modules\Admin\Base\BaseForm;
use Modules\Admin\Entities\Roles;
use Modules\Admin\Traits\Form\HasFormGrid;

class AdminUserForm extends BaseForm
{
    use HasFormGrid;

    protected $sextype;

    protected $rules = [
        'name' => 'required|unique:users,name,{id}',
        'email' => 'required|email|unique:users,email,{id}',
        'mobile' => 'required|unique:users,mobile,{id}',
//        'sex' => 'required|integer|in:{sex_type}',
        'group' => 'required|exists:roles,id',
        'password' => 'confirmed',
        'cover' => 'file|image|mimes:jpeg,bmp,png',
        //'permissions' => 'array',
    ];

    protected $rules_replace = [
        '{id}' => 'getValueId',
//        '{sex_type}' => 'getValueSexType',
    ];

    public function buildForm()
    {
        $this->groupFields(function () {
            $this->add('name', 'text', ['label' => trans('admin::users.name')]);
            $this->add('email', 'text', ['label' => trans('admin::users.email')]);
            $this->add('mobile', 'text', ['label' => trans('admin::users.mobile')]);
        }, null, 'three');

        $this->add('password', 'repeated', [
            'first_options' => [
                'value' => '',
                'label' => trans('admin::users.password'),
                'help_block' => ['text' => trans('admin::users.help.password_change')],
            ],
            'second_options' => [
                'label' => trans('admin::users.password_confirm'),
            ],
            'second_name' => 'password_confirmation',
            'type' => 'password',
            'wrapper' => ['class' => 'two fields clearfix'],
        ]);

        $this->add('group', 'select', [
            'label' => trans('admin::users.group'),
            'choices' => Roles::admin()->selectList(),
            'empty_value' => trans('admin::common.-- select one --'),
            'default_value' => $this->getModel() ? $this->getModel()->roles->first()->id : null,
        ]);

        $this->add('cover', 'file', ['label' => trans('admin::users.profile photo'), 'wrapper' => ['skip' => true], 'attr' => ['class' => 'dropify', 'data-default-file' => ($this->getModel()) ? $this->getModel()->logo : '']]);

        $this->submitButton();

        if (!$this->getModel()) {
            $this->rules['password'] = 'required|confirmed';
        }

        $this->appendRules();
    }

    protected function getValueId()
    {
        return (\is_object($this->getModel())) ? $this->getModel()->id : '';
    }
}
