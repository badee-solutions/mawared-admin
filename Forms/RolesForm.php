<?php

/*
 * Copyright (c) 2017 Salah Alkhwlani <yemenifree@yandex.com>
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Modules\Admin\Forms;

use Modules\Admin\Base\BaseForm;
use Modules\Admin\Entities\Roles;

class RolesForm extends BaseForm
{
    public function buildForm()
    {
        $this->add('name', 'text', ['label' => trans('admin::roles.name'), 'rules' => ['required']]);
        $this->add('permissions', 'select', ['label' => trans('admin::roles.permissions'),
            'rules' => 'array',
            'attr' => ['multiple' => 'multiple', 'class' => 'optgroup'],
            'choices' => Roles::buildPermission(),
            'selected' => ($this->getModel()) ? \array_keys($this->getModel()->permissions) : [],
            ]);
        $this->add('is_admin', 'checkbox', ['label' => trans('admin::roles.is_admin'), 'rules' => 'boolean', 'help_block' => ['text' => trans('admin::roles.help.is_admin')], 'attr' => ['class' => 'js-switch remove-checkbox']]);
        $this->submitButton();
    }
}
