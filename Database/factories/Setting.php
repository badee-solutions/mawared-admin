<?php

/*
 * Copyright (c) 2017 Salah Alkhwlani <yemenifree@yandex.com>
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

use Faker\Generator as Faker;

$factory->define(\Larapacks\Setting\Models\Setting::class, function (Faker $faker) {
    return [
        'key' => $faker->slug,
        'value' => [],
    ];
});
