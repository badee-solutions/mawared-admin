<?php

/*
 * Copyright (c) 2017 Salah Alkhwlani <yemenifree@yandex.com>
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

use Faker\Generator as Faker;
use Modules\Admin\Entities\Roles;

$factory->define(Roles::class, function (Faker $faker) {
    return [
        'name' => $faker->jobTitle,
        'permissions' => [],
        'is_admin' => false,
    ];
});
