<?php

/*
 * Copyright (c) 2017 Salah Alkhwlani <yemenifree@yandex.com>
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

use App\User;
use Faker\Generator as Faker;

$factory->define(User::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->valid(function ($email) {
            return !User::whereEmail($email)->exists();
        })->safeEmail,
        'mobile' => $faker->valid(function ($mobile) {
            return !User::whereMobile($mobile)->exists();
        })->numerify('050#######'),
        'country_code' => 'SA',
        'password' => 123,
        'active' => $faker->boolean(),
    ];
});
