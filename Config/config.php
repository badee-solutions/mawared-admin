<?php

/*
 * Copyright (c) 2017 Salah Alkhwlani <yemenifree@yandex.com>
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

return [
    'name' => 'Admin',
    'datatable' => [
        'initComplete' => 'admin::datatable.initComplete',
    ],
    'controller' => [
        'roles' => '\Modules\Admin\Http\Controllers',
        'users' => '\Modules\Admin\Http\Controllers',
        'login' => '\Modules\Admin\Http\Controllers',
    ],
    'menu' => [
        [
            'name' => 'common.dashboard',
            'route' => 'admin.dashboard',
            'icon' => 'pe-7s-graph',
            'order' => 0,
        ],
        [
            'name' => 'common.system',
            'slug' => 'system',
            'icon' => 'icon-settings',
            'order' => 999,
        ],
        [
            'name' => 'common.administrators',
            'slug' => 'admins',
            'icon' => 'icon-user',
            'order' => 10,
        ],
        [
            'name' => 'common.admins',
            'route' => 'admin.users.index',
            'icon' => 'icon-user',
            'order' => 10,
            //"parent" => "admins",
        ],
        [
            'name' => 'common.groups',
            'route' => 'admin.roles.index',
            'icon' => 'icon-user',
            'parent' => 'admins',
            'hide' => true,
        ],
    ],
];
