<?php

/*
 * Copyright (c) 2017 Salah Alkhwlani <yemenifree@yandex.com>
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

return [
    'defaults' => [
        'wrapper_class' => 'form-group',
        'wrapper_error_class' => 'has-error',
        'label_class' => 'control-label',
        'field_class' => 'form-control',
        'help_block_class' => 'help-block',
        'error_class' => 'text-danger',
        'required_class' => 'required',

        // Override a class from a field.
        //'text'                => [
        //    'wrapper_class'   => 'form-field-text',
        //    'label_class'     => 'form-field-text-label',
        //    'field_class'     => 'form-field-text-field',
        //]
        //'radio'               => [
        //    'choice_options'  => [
        //        'wrapper'     => ['class' => 'form-radio'],
        //        'label'       => ['class' => 'form-radio-label'],
        //        'field'       => ['class' => 'form-radio-field'],
        //],
    ],
    // Templates
    'form' => 'admin::form.form',
    'text' => 'admin::form.text',
    'textarea' => 'admin::form.textarea',
    'button' => 'admin::form.button',
    'buttongroup' => 'admin::form.buttongroup',
    'radio' => 'admin::form.radio',
    'checkbox' => 'admin::form.checkbox',
    'select' => 'admin::form.select',
    'choice' => 'admin::form.choice',
    'repeated' => 'admin::form.repeated',
    'child_form' => 'admin::form.child_form',
    'collection' => 'admin::form.collection',
    'static' => 'admin::form.static',

    // Remove the admin::form. prefix above when using template_prefix
    'template_prefix' => '',

    'default_namespace' => '',

    'custom_fields' => [
        'h1' => Modules\Admin\Forms\Fields\SpaceType::class,
        'map' => Modules\Admin\Forms\Fields\GoogleMap::class,
        'start-loop' => Modules\Admin\Forms\Fields\StartLoopChildForm::class,
        'close-loop' => Modules\Admin\Forms\Fields\CloseLoopChildForm::class,
//        'datetime' => App\Forms\Fields\Datetime::class
    ],
];
