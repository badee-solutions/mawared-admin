<?php

/*
 * Copyright (c) 2017 Salah Alkhwlani <yemenifree@yandex.com>
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Modules\Admin\Scope;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Scope;

class UserRolesScope implements Scope
{
    protected $roles = [];

    public function __construct($roles)
    {
        $this->roles = \is_array($roles) ? $roles : [$roles];
    }

    /**
     * Apply the scope to a given Eloquent query builder.
     *
     * @param \Illuminate\Database\Eloquent\Builder $builder
     * @param \Illuminate\Database\Eloquent\Model   $model
     */
    public function apply(Builder $builder, Model $model)
    {
        $builder->whereHas('roles', function ($query) {
            $query->whereIn($this->getColumn(), $this->roles);
        });
    }

    private function getColumn()
    {
        return ($this->roles == \array_filter($this->roles, 'is_numeric')) ? 'role_id' : 'slug';
    }
}
