<?php

/*
 * Copyright (c) 2017 Salah Alkhwlani <yemenifree@yandex.com>
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Modules\Admin\Providers;

use Closure;
use Illuminate\Support\ServiceProvider;
use Route;

class RouteMacroServiceProvider extends ServiceProvider
{
    /**
     * Register the application services.
     */
    public function register()
    {
        Route::macro('routeIfConfigEnable', function ($config, Closure $callback, $defualt = false) {
            if (!config($config, $defualt) || (isset($config['hide']) && $config['hide'] == true)) {
                return;
            }

            $callback();
        });
    }
}
