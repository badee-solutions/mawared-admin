<?php

/*
 * Copyright (c) 2017 Salah Alkhwlani <yemenifree@yandex.com>
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Modules\Admin\Providers;

use Illuminate\Support\ServiceProvider;
use Validator;

class ValidationServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Boot the application events.
     */
    public function boot()
    {
        Validator::extend('uuid', function ($attribute, $value, $parameters, $validator) {
            return \is_string($value) && \preg_match('/^[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}$/', $value) === 1;
        });

        Validator::extendImplicit('required_image', function ($attribute, $value, $parameters, $validator) {
            return !(!request()->hasFile($attribute) && empty(\trim(SettingOption($attribute))));
        });

        $this->app['eloquence.mutator']->macro('nullifyEmpty', function ($value) {
            if (\is_array($value)) {
                return \array_map([$this, 'nullifyEmpty'], $value);
            }

            return (\strlen($value)) ? $value : null;
        });

        $this->app['eloquence.mutator']->macro('falseifyEmpty', function ($value) {
            if (\is_array($value)) {
                return \array_map([$this, 'falseifyEmpty'], $value);
            }

            return (\strlen($value)) ? $value : false;
        });
    }

    /**
     * Register the service provider.
     */
    public function register()
    {
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [];
    }
}
