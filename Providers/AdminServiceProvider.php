<?php

/*
 * Copyright (c) 2017 Salah Alkhwlani <yemenifree@yandex.com>
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Modules\Admin\Providers;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factory;
use Illuminate\Support\ServiceProvider;
use Modules\Admin\Base\BaseForm;
use Modules\Admin\Http\Middleware\DecodeFakeIdMiddleware;
use Modules\Admin\Http\Middleware\SentinelAdminPermissions;
use Modules\Admin\Http\Middleware\SentinelAdminUser;
use Modules\Admin\Http\Middleware\SentinelApiRoleMiddleware;
use Modules\Admin\Http\Middleware\SentinelRoleMiddleware;

class AdminServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Boot the application events.
     */
    public function boot()
    {
        $this->registerTranslations();
        $this->registerConfig();
        $this->registerViews();

        $this->registerFactories();
        $this->loadMigrationsFrom(__DIR__ . '/../Database/Migrations');

        \setlocale(LC_ALL, \Config::get('app.timezone'));
        Carbon::setLocale(\Config::get('locale'));
        BaseForm::setUI('bootstrap');

        config('datatables.script_template', 'admin::datatable');

        // todo :: fix doesnot load setting from cache
        if (\class_exists('Setting') && !app()->runningInConsole()) {
            $setting = \Setting::get('app');
            $name = SettingOption('app.name', null, true);
            \Config::set('app.name', $name);
            \Config::set('mail.from.address', $setting['email']);
            \Config::set('mail.from.name', $setting['name'][\App::getLocale()]);
        }
    }

    /**
     * Register translations.
     */
    public function registerTranslations()
    {
        $langPath = base_path('resources/lang/modules/admin');

        if (\is_dir($langPath)) {
            $this->loadTranslationsFrom($langPath, 'admin');
        } else {
            $this->loadTranslationsFrom(__DIR__ . '/../Resources/lang', 'admin');
        }
    }

    /**
     * Register config.
     */
    protected function registerConfig()
    {
        $this->publishes([__DIR__ . '/../Config/config.php' => config_path('admin.php')]);
        $this->mergeConfigFrom(__DIR__ . '/../Config/config.php', 'admin');
    }

    /**
     * Register views.
     */
    public function registerViews()
    {
        $viewPath = base_path('resources/views/modules/admin');

        $sourcePath = __DIR__ . '/../Resources/views';

        $this->publishes([$sourcePath => $viewPath]);

        $this->loadViewsFrom(\array_merge(\array_map(function ($path) {
            return $path . '/modules/admin';
        }, \Config::get('view.paths')), [$sourcePath]), 'admin');
    }

    /**
     * Register an additional directory of factories.
     *
     * @source https://github.com/sebastiaanluca/laravel-resource-flow/blob/develop/src/Modules/ModuleServiceProvider.php#L66
     */
    public function registerFactories()
    {
        if (app()->runningInConsole()) {
            app(Factory::class)->load(__DIR__ . '/../Database/factories');
        }
    }

    /**
     * Register the service provider.
     */
    public function register()
    {
        $this->app->register(ValidationServiceProvider::class);
        $this->app->register(HelperServiceProvider::class);
        $this->app->register(RouteMacroServiceProvider::class);

        $this->app->register(\Cog\Flag\Providers\FlagServiceProvider::class);
        $this->app->register(\Lavary\Menu\ServiceProvider::class);
        $this->app->register(\Prettus\Repository\Providers\RepositoryServiceProvider::class);

        $loader = \Illuminate\Foundation\AliasLoader::getInstance();

        $loader->alias('Menu', \Lavary\Menu\Facade::class);

        $loader->alias('Form', \Collective\Html\FormFacade::class);
        $loader->alias('HTML', \Collective\Html\HtmlFacade::class);

        $this->app['router']->aliasMiddleware('admin', SentinelAdminUser::class);
        $this->app['router']->aliasMiddleware('permissions', SentinelAdminPermissions::class);
        $this->app['router']->aliasMiddleware('fakeids', DecodeFakeIdMiddleware::class);
        $this->app['router']->aliasMiddleware('role', SentinelRoleMiddleware::class);
        $this->app['router']->aliasMiddleware('api.role', SentinelApiRoleMiddleware::class);
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [];
    }
}
