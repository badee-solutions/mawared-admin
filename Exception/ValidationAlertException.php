<?php

/*
 * Copyright (c) 2017 Salah Alkhwlani <yemenifree@yandex.com>
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Modules\Admin\Exception;

use Exception;

class ValidationAlertException extends Exception
{
    /**
     * The validator instance.
     *
     * @var \Illuminate\Contracts\Validation\Validator
     */
    public $validator;

    /**
     * Create a new exception instance.
     *
     * @param \Illuminate\Contracts\Validation\Validator $validator
     */
    public function __construct($validator)
    {
        parent::__construct('The given data failed to pass validation.');

        $this->validator = $validator;
    }

    public function getFirstError()
    {
        return $this->validator->errors()->first();
    }
}
