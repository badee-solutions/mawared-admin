<?php if ($showLabel && $showField): ?>
<?php if (isset($options['parent_wrapper']) && $options['parent_wrapper'] !== false): ?>
<div class="<?php echo $options['parent_wrapper']; ?>" >
    <?php endif; ?>

    <?php if ($options['wrapper'] !== false): ?>
<div <?php echo $options['wrapperAttrs']; ?> >
    <?php endif; ?>
    <?php endif; ?>

    <?php if ($showField): ?>
        <?php echo Form::checkbox($name, $options['value'], $options['checked'], $options['attr']); ?>
    <?php endif; ?>

    <?php if ($showLabel && $options['label'] !== false && $options['label_show']): ?>
        <?php echo Form::customLabel($name, $options['label'], $options['label_attr']); ?>
    <?php endif; ?>
    <?php include 'help_block.php'; ?>
    <?php include 'errors.php'; ?>

    <?php if ($showLabel && $showField): ?>
    <?php if ($options['wrapper'] !== false): ?>
</div>
<?php endif; ?>
    <?php if (isset($options['parent_wrapper']) && $options['parent_wrapper'] !== false): ?>
</div>
<?php endif; ?>
<?php endif; ?>
