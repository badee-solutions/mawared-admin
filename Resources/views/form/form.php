<?php if ($showStart): ?>
    <?php echo Form::open($formOptions); ?>
<?php endif; ?>

<?php if ($showFields): ?>
    <?php foreach ($fields as $field): ?>
    	<?php if (!\in_array($field->getName(), $exclude)) {
    ?>
        	<?php echo $field->render(); ?>
		<?php
} ?>
    <?php endforeach; ?>
<?php endif; ?>

<?php if ($showEnd): ?>
    <?php echo Form::close(); ?>
<?php endif; ?>
