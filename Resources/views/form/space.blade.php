<div class="clearfix"></div>
@if (isset($options['label']) && !empty($options['label']))
    <h6 class="txt-dark capitalize-font">
        @if (isset($options['icon']) && !empty($options['icon']))
            <i class="{{ $options['icon'] }} mr-10"></i>
        @endif
        {{ $options['label'] }}
    </h6>
@endif
<hr>
