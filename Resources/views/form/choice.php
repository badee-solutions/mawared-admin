<?php if ($showLabel && $showField): ?>
<?php if (isset($options['parent_wrapper']) && $options['parent_wrapper'] !== false): ?>
<div class="<?php echo $options['parent_wrapper']; ?>">
    <?php endif; ?>
    <?php if ($options['wrapper'] !== false): ?>
    <div <?php echo $options['wrapperAttrs']; ?> >
        <?php endif; ?>
        <?php endif; ?>

        <?php if ($showLabel && $options['label'] !== false && $options['label_show']): ?>
            <?php echo Form::customLabel($name, $options['label'], $options['label_attr']); ?>
        <?php endif; ?>
        <?php if (isset($options['wrapper_child']) && $options['wrapper_child'] !== false): ?>
        <div class="<?php echo $options['wrapper_child']; ?>">
            <?php endif; ?>
            <?php if ($showField): ?>
                <?php foreach ((array) $options['children'] as $child): ?>
                    <?php echo $child->render(['selected' => $options['selected']], true, true, false); ?>
                <?php endforeach; ?>
                <?php include 'help_block.php'; ?>
            <?php endif; ?>
            <?php if (isset($options['wrapper_child']) && $options['wrapper_child'] !== false): ?>
        </div>
    <?php endif; ?>

        <?php include 'errors.php'; ?>

        <?php if ($showLabel && $showField): ?>
        <?php if ($options['wrapper'] !== false): ?>
    </div>
<?php endif; ?>
    <?php if (isset($options['parent_wrapper']) && $options['parent_wrapper'] !== false): ?>
</div>
<?php endif; ?>
<?php endif; ?>
