<?php if ($showLabel && $showField): ?>
    <?php if ($options['wrapper'] !== false): ?>
    <div <?php echo $options['wrapperAttrs']; ?> >
    <?php endif; ?>
<?php endif; ?>

<?php if ($showLabel && $options['label'] !== false && $options['label_show']): ?>
    <?php echo Form::customLabel($name, $options['label'], $options['label_attr']); ?>
<?php endif; ?>

<?php if ($showField): ?>
    <?php foreach ((array) $options['children'] as $child): ?>
        <?php if (!\in_array($child->getRealName(), (array) $options['exclude'])) {
    ?>
            <?php echo $child->render(); ?>
        <?php
} ?>
    <?php endforeach; ?>

    <?php include 'help_block.php'; ?>

<?php endif; ?>

<?php include 'errors.php'; ?>

<?php if ($showLabel && $showField): ?>
    <?php if ($options['wrapper'] !== false): ?>
    </div>
    <?php endif; ?>
<?php endif; ?>
