@if ($showLabel && $showField)
@if ($options['wrapper'] !== false)
<div {{ $options['wrapperAttrs'] }} >
    @endif
    @endif

    @if ($showLabel && $options['label'] !== false && $options['label_show'])
        <?= Form::customLabel($name, $options['label'], $options['label_attr']) ?>
    @endif

    @if ($showField)
        <?= Form::input('text', $name, $options['value'], $options['attr']) ?>
        @include('admin::form.help_block')
    @endif

    <div id="{{  $options['attr']['map'] }}" style="width: 100%; height: 400px;"></div>

    @include('admin::form.errors')

    @if ($showLabel && $showField)
    @if ($options['wrapper'] !== false)
</div>
@endif
@endif

@push('scripts')
<script type="text/javascript">
    jQuery().ready(function () {
        jQuery('#{{ $options['attr']['map'] }}').locationpicker({
            location: {latitude: jQuery('#{{ $options['lat']['id'] }}').val(), longitude: jQuery('#{{ $options['lon']['id'] }}').val()},
            radius: 300,
            inputBinding: {
                latitudeInput: jQuery('#{{ $options['lat']['id'] }}'),
                longitudeInput: jQuery('#{{ $options['lon']['id'] }}'),
                locationNameInput: jQuery('#{{ $options['attr']['id'] }}')
            },
            enableAutocomplete: true,
        });
    });
</script>
@endpush