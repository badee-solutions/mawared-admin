<?php if ($showLabel && $showField): ?>
    <?php if ($options['wrapper'] !== false): ?>
    <div <?php echo $options['wrapperAttrs']; ?> >
    <?php endif; ?>
<?php endif; ?>

<?php if ($showField): ?>
    <?php echo $options['children']['first']->render([], true, true, false); ?>
    <?php echo $options['children']['second']->render([], true, true, false); ?>

    <?php include 'help_block.php'; ?>

<?php endif; ?>

<?php if ($showError && isset($errors)): ?>
    <?php echo $options['children']['first']->render([], false, false, true); ?>
    <?php echo $options['children']['second']->render([], false, false, true); ?>
<?php endif; ?>

<?php if ($showLabel && $showField): ?>
    <?php if ($options['wrapper'] !== false): ?>
    </div>
    <?php endif; ?>
<?php endif; ?>
