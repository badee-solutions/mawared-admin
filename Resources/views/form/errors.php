<?php if ($showError && isset($errors)): ?>
    <?php foreach ($errors->get($nameKey) as $err): ?>
        <div <?php echo $options['errorAttrs']; ?>><?php echo $err; ?></div>
    <?php endforeach; ?>
<?php endif; ?>

