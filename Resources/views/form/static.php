<?php if ($showLabel && $showField): ?>
    <?php if ($options['wrapper'] !== false): ?>
    <div <?php echo $options['wrapperAttrs']; ?> >
    <?php endif; ?>
<?php endif; ?>

<?php if ($showLabel && $options['label'] !== false && $options['label_show']): ?>
    <?php echo Form::customLabel($name, $options['label'], $options['label_attr']); ?>
<?php endif; ?>

<?php if ($showField): ?>
    <<?php echo $options['tag']; ?> <?php echo $options['elemAttrs']; ?>><?php echo $options['value']; ?></<?php echo $options['tag']; ?>>

    <?php include 'help_block.php'; ?>

<?php endif; ?>


<?php if ($showLabel && $showField): ?>
    <?php if ($options['wrapper'] !== false): ?>
    </div>
    <?php endif; ?>
<?php endif; ?>
