<?php if ($options['wrapper'] !== false): ?>
    <div <?php echo $options['wrapperAttrs']; ?> >
<?php endif; ?>

    <?php if (!$options['splitted']): ?>
        <div class="btn-group btn-group-<?php echo $options['size']; ?>">
    <?php endif; ?>

        <?php foreach ($options['buttons'] as $button): ?>
            <?php echo Form::button($button['label'], $button['attr']); ?>
        <?php endforeach; ?>

    <?php if (!$options['splitted']): ?>
        </div>
    <?php endif; ?>


<?php if ($options['wrapper'] !== false): ?>
    </div>
<?php endif; ?>
