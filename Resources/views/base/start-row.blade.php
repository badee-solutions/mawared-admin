<div class="row">
    <div class="col-sm-12">
        <div class="panel panel-default card-view">
            @if(isset($panel_title))
                <div class="panel-heading">
                    <div class="pull-left">
                        <h6 class="panel-title txt-dark">{{ $panel_title }}</h6>
                    </div>
                    <div class="clearfix"></div>
                </div>
            @endif

            <div class="panel-wrapper collapse in">
                <div class="panel-body">