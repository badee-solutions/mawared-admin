<?php
/**
 * Created by PhpStorm.
 * User: salah
 * Date: 25 ماي، 2016 م
 * Time: 12:24 م
 */
?>
@extends('admin::layouts.admin')
@section('content')

    @include('admin::common.start_form',['form_title' => $title,'hasLang' => $form->getData('hasLang',false),'form' => $form])

    {!! form($form) !!}

    <div class="form-group mb-0 pull-right">
        <button type="button" onclick="window.history.back();" placeholder="{{ trans('admin::common.cancel') }}" class="btn btn-warning btn-anim">
            <i class="icon-close"></i>
            <span class="btn-text">{{ trans('admin::common.cancel') }}</span>
        </button>
    </div>

    @include('admin::common.end_form')

@endsection