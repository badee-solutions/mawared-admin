@extends('admin::layouts.full-width')
@section('content')
    <div class="table-struct full-width full-height">
        <div class="table-cell vertical-align-middle">
            <div class="auth-form  ml-auto mr-auto no-float">
                <div class="panel panel-default card-view mb-0">
                    <div class="panel-wrapper collapse in">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-sm-12 col-xs-12 text-center">
                                    <h3 class="mb-20 txt-danger">{{ trans('admin::error.404.title') }}</h3>
                                    <p class="font-18 txt-dark mb-15">{{ trans('admin::error.404.body') }}</p>
                                    <p>{{ trans('admin::error.404.extend') }}</p>
                                    <a class="btn btn-success btn-icon right-icon btn-rounded mt-30" href="{{ URL::to('admin') }}">
                                        <span>{{ trans('admin::common.back to home') }}</span>
                                        <i class="fa fa-space-shuttle"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection