<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 5/31/16
 * Time: 2:39 PM
 */
?>
<div class="form-group upload-inline">
    <div class="input-group">
        <span class="input-group-btn">
            <a id="lfm" data-input="thumbnail" data-preview="holder" class="btn btn-primary">
                <i class="fa fa-picture-o"></i> Choose
            </a>
        </span>
        {!! BootForm::text($label, $input)->placeholder($label)->id('thumbnail') !!}
        {{--<input id="thumbnail" class="form-control" type="text" name="filepath">--}}
    </div>
    <img id="holder" style="margin-top:15px;max-height:100px;">
</div>
@push('scripts')
<script type="application/javascript">
    $().ready(function () {
        $('#lfm').filemanager('image');
    });
</script>
@endpush