@include('admin::base.start-row')

@if (isset($form_title) && !empty($form_title) && !isset($hide_form_title))
    <h6 class="txt-dark capitalize-font">
        <i class="icon-pencil mr-10"></i>
        {{ $form->getData('title',$form_title) }}
        @if($form->getData('hasLang',false))
            <div class="lang-switch-box">
                <div class="btn-group">
                    @foreach(\Localization::getSupportedLocales() as $locale)
                        <button type="button"
                                class="btn {{ $locale->key() == \Localization::getCurrentLocale() ? 'btn-success' : 'btn-default' }}"
                                data-key="{{ $locale->key() }}">{{ $locale->native() }}</button>
                    @endforeach
                </div>
                <input type="hidden" value="{{  \Localization::getCurrentLocale() }}" name="lang-switch"
                       id="lang-switch">
            </div>
        @endif
    </h6>
    <hr style="margin-top: 0;">
@endif

<div class="row">
    <div class="col-md-12">
        <div class="form-wrap">