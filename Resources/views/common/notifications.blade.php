@foreach(Sentinel::getUser()->notifications()->with(['body','from'])->orderby('id','desc')->paginate(10) as $noti)
    <li>
        <i class="si si-wallet text-success"></i>
        <a class="font-w600" style="display: inline-block;" href="{{ $noti->url  }}"> {{ $noti->notify_body  }}</a>
        <div><a href="{{ UrlLang(env('APP_ADMIN_PREFIX','admin').'/user/'.$noti->from->id) }}">{{ $noti->from->username }}</a></div>
        <div>
            <small class="text-muted">{{ $noti->created_at->diffForHumans() }}</small>
        </div>
    </li>
@endforeach

