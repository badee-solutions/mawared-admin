<!-- Top Menu Items -->
<nav class="navbar navbar-inverse navbar-fixed-top">
    <a id="toggle_nav_btn" class="toggle-left-nav-btn inline-block mr-20 pull-left" href="javascript:void(0);">
        <i class="fa fa-bars"></i>
    </a>
    <a href="{{ url(env('APP_ADMIN_PREFIX','admin')) }}">
        <img class="brand-img pull-left" src="{{ asset('control/dist/img/logo.png') }}" />
    </a>
    <ul class="nav navbar-right top-nav pull-right">
        <li class="dropdown">
            <a href="#" class="dropdown-toggle pr-0" data-toggle="dropdown">
                <img class="user-auth-img img-circle" src="{{ (!\Sentinel::guest() && isset(\Sentinel::getUser()->logo)) ? \Sentinel::getUser()->logo : asset('control/dist/img/user1.png') }}" />
                <span> {{ Sentinel::getUser()->name }}</span>
                {{--<span class="user-online-status"></span>--}}
            </a>
            <ul class="dropdown-menu user-auth-dropdown" data-dropdown-in="fadeIn" data-dropdown-out="fadeOut">
                <li><a href="{{ route('admin.users.edit',['id' => Sentinel::getUser()]) }}"><i class="fa fa-fw fa-user"></i> {{ trans('user.profile') }}</a></li>
                <li><a href="{{ route('admin.settings.app') }}"><i class="fa fa-fw fa-user"></i> {{ trans('user.settings') }}</a></li>
                <li><a href="{{ route('web.auth.logout') }}"><i class="fa fa-fw fa-power-off"></i> {{ trans('user.logout') }}</a></li>
            </ul>
        </li>
    </ul>
</nav>
<!-- /Top Menu Items -->
