<?php
/**
 * Created by PhpStorm.
 * User: salah
 * Date: 25 ماي، 2016 م
 * Time: 12:16 م
 */
?>

<!-- Left Sidebar Menu -->
<div class="fixed-sidebar-left">
    <ul class="nav navbar-nav side-nav nicescroll-bar">
        @include('admin::common.menu', array('items' => Menu::get('Admin')->roots()))
    </ul>
</div>
