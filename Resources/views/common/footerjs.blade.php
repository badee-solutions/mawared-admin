<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 26 ماي، 2016 م
 * Time: 10:10 ص
 */
?>
<div id="alert-sure" class="modal fade in alert-modal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-body">
                <div class="icon-alert"></div>
                <h3>Warning Massage</h3>
                <p>Are you sure you want to proceed?</p>
            </div>
            <div class="modal-footer">
                <a href="#" data-dismiss="modal" class="alert-btn-close">Cancel</a>
                <a href="#" class="alert-btn-ok">Procced</a>
            </div>
        </div>

    </div>
</div>

{!! Assets::group('admin')->add($assets)->js() !!}
@stack('scripts')
<script>
    $(function () {
        if (typeof $.fn.dataTableExt !== 'undefined') {
            $.fn.dataTableExt.sErrMode = 'none';
        }
    });
</script>
</body>
</html>