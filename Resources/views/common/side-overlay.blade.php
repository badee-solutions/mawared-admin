<!-- Side Overlay-->
<div id="side-overlay">
    <!-- Side Overlay Scroll Container -->
    <div id="side-overlay-scroll">
        <!-- Side Header -->
        <div class="side-header side-content">
            <!-- Layout API, functionality initialized in App() -> uiLayoutApi() -->
            <button class="btn btn-default pull-right" type="button" data-toggle="layout"
                    data-action="side_overlay_close">
                <i class="fa fa-times"></i>
            </button>
            <span>
                <!--<img class="img-avatar img-avatar32" src="assets/img/avatars/avatar10.jpg" alt="">-->
                <a href="{{ UrlLang(env('APP_ADMIN_PREFIX','admin').'/notifications') }}" class="font-w600 push-10-l">{{ trans('admin.common.notification') }} </a>
            </span>
        </div>
        <!-- END Side Header -->

        <!-- Side Content -->
        <div class="side-content remove-padding-t">
            <!-- Notifications -->
            <div class="block pull-r-l">
                <div class="block-header bg-gray-lighter">
                    <ul class="block-options">
                        <li>
                            <a class="ajax" data-nonconfirm="1" data-page="1" href="{{ UrlLang(env('APP_ADMIN_PREFIX','admin').'/lastactivity') }}"><i class="si si-refresh"></i></a>
                        </li>
                        <li>
                            <button type="button" data-toggle="block-option" data-action="content_toggle"></button>
                        </li>
                    </ul>
                    <h3 class="block-title">{{ trans('admin.common.recent activity') }}</h3>
                </div>
                <div class="block-content">
                    <!-- Activity List -->
                    <div class="container-noti">
                        <ul class="list list-activity" id="list-activity">
                            @include('admin.common.notifications')
                        </ul>
                    </div>
                    <div class="text-center pag-list-activity">
                        <small><a class="ajax list-activity-btn wihtout-confi" data-nonconfirm="1" data-page="2" href="{{ UrlLang(env('APP_ADMIN_PREFIX','admin').'/lastactivity') }}">{{ trans('admin.common.load-more.Load More..') }}</a></small>
                    </div>
                </div>
            </div>
            <!-- END Notifications -->
        </div>
        <!-- END Side Content -->
    </div>
    <!-- END Side Overlay Scroll Container -->
</div>
<!-- END Side Overlay -->