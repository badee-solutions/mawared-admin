<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 26 ماي، 2016 م
 * Time: 10:08 ص
 */
?>
<!doctype html>
<html lang="{{ Config::get('app.locale') }}" dir="{{ \Localization::getCurrentLocaleEntity()->direction() }}">
<head>
    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
    {!! app('seotools')->generate() !!}
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
    <meta name="viewport" content="width=device-width"/>
    <meta name="_token" content="{!! csrf_token() !!}"/>
    <meta name="_error_class" content="alert alert-danger"/>
    <base href="{{ URL::to('/') }}">
    {!! Assets::group('admin')->add($assets)->css() !!}
    <link rel="icon" type="image/png" href="{{ asset('front/images/favicon.png') }}">

<body class="{{ $class or '' }} {{ \Route2Class::generateClassString() }}" id="content-body">
<!--Preloader-->
<div class="preloader-it">
    <div class="la-anim-1"></div>
</div>
<!--/Preloader-->
