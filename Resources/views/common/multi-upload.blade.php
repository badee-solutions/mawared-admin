<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 5/31/16
 * Time: 2:39 PM
 */
?>

@if(is_object($values) && count($values) > 0)
    <table class="table table-hover">
        <thead>
        <tr>
            <th>{{trans('admin.common.file name')}}</th>
            <th>{{trans('admin.common.view')}}</th>
        </tr>
        </thead>
        <tbody>
        @foreach($values as $file)
            {!! BootForm::hidden($input.'[]')->value($file->filename)->class('file-'.$file->id) !!}
            <tr class="file-{{$file->id}}">
                <td>{{$file->filename}}</td>
                <td>
                    <a href="{{carfiles($file->filename)}}" class="image-popups" data-effect="mfp-3d-unfold"><i class="fa fa-eye"></i> </a>
                    <a href="{{$file->remove_link}}" id="file-{{$file->id}}" class="ajax"><i class="fa fa-close"></i> </a>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
@endif
<div id="{{$id}}" data-input="{{$input}}" class="dropzone needsclick">
    <div class="dz-message needsclick">
        Drop files here or click to upload.<br/>
    </div>
    {{--<div class="fallback">--}}
    {{--<input name="file" type="file" multiple />--}}
    {{--</div>--}}
</div>

@push('scripts')
<script type="application/javascript">
    $().ready(function () {
        var myDropzone = new Dropzone("#{{$id}}", {
            //paramName: "{{$input}}",
            uploadMultiple: true,
            addRemoveLinks: true,
            autoProcessQueue: true,
            //dictDefaultMessage: '',
            url: "{{UrlLang('ajax/upload')}}",
            init: function () {
                this.on("addedfile", function (file) {

                    NProgress.start();
                });
                this.on("removedfile", function (fileinfo) {
                    console.log($.md5(fileinfo.name));
                    $('input[id="' + $.md5(fileinfo.name) + '"]').remove();
                });
                this.on("complete", function (file) {
                    NProgress.done();
                });
                this.on("error", function (file) {
                    notie.alert(3, 'Sorry, try upload file again', 5);
                });
                this.on("success", function (file, res) {
                    $('<input>').attr({
                        type: 'hidden',
                        id: $.md5(file.name),
                        name: '{{$input}}[]',
                        value: res.filename
                    }).appendTo('.ajax');
                });
            }
        });
    });
</script>
@endpush
