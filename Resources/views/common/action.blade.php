<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 5/31/16
 * Time: 10:10 AM
 */
?>
@if(count($actions) > 0)
    <div class="dropdown">
        <button class="btn btn-default btn-sm dropdown-toggle" type="button" id="dropdownMenu{{ $info->id }}"
                data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
            {{ trans('common.control') }}
            <span class="caret"></span>
        </button>
        <ul class="dropdown-menu" aria-labelledby="dropdownMenu{{ $info->id }}">
            @foreach($actions as $action_info)
                <li><a href="{{$action_info['url']}}" class="{{ $action_info['class'] or '' }}" target="{{ $action_info['target'] or '' }}"
                    @foreach(array_except($action_info,['url','class','target']) as $name => $info) data-{{$name}}="{{$info}}" @endforeach >{{$action_info['name']}}</a></li>
            @endforeach
        </ul>
    </div>
@else
    -
@endif
