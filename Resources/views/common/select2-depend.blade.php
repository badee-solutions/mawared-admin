<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 6/16/16
 * Time: 12:25 PM
 */
?>

var depdrop_{{$name}} = {"depends":["{{$depends}}"],"url":"{{$url}}","loadingText":"{{trans('admin.common.Loading')}}","initialize":true};
kvInitPlugin('#{{$name}}', function(){
if (jQuery('#{{$name}}').data('depdrop')) { jQuery('#{{$name}}').depdrop('destroy'); }
jQuery('#{{$name}}').depdrop(depdrop_{{$name}});

});
kvInitPlugin(jQuery('#{{$name}}').kvSelector(), function(){
if (jQuery('#{{$name}}').data('select2')) { jQuery('#{{$name}}').select2('destroy'); }
jQuery.when(jQuery('#{{$name}}').select2(select2_980572ea)).done(initS2Loading('model','s2options_d6851687'));
});
initDepdropS2('{{$name}}',"{{trans('admin.common.Loading')}}");
