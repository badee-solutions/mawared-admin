<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 5/31/16
 * Time: 2:39 PM
 */
?>
<div class="upload-files">
    {!! BootForm::hidden($input) !!}
    @if(isset($is_img))
        <div id="img-thumb-preview">
            <img id="img-thumb-{{$id}}" data-input="photo" class="avatar border-gray"
                 src="{{ !empty($old_img) ? $old_img : asset('public/'.env('APP_ADMIN_PREFIX','admin').'/assets/img/default-avatar.png') }}">
        </div>
    @endif
    @if(!isset($nbtn))
        <div id="{{$id}}" data-input="{{$input}}"
             class="btn btn-default margin-t-5 needsclick icobutton icobutton--thumbs-up">
            <i class="fa fa-upload"></i> {{ $upload or trans('userprofile.upload.Upload') }}
        </div>
        @push('scripts')
        <script type="application/javascript">
            $().ready(function () {
                Dropzone.autoDiscover = false;
                var myDropzone = new Dropzone("#{{$id}}", {
                    uploadMultiple: false,
                    addRemoveLinks: false,
                    dictDefaultMessage: '',
//                    maxFiles: 1,
                    url: "{{UrlLang('ajax/upload')}}",
                    init: function () {
                        this.on("maxfilesexceeded", function(file){
                            this.removeAllFiles();
                            this.addFile(file);
                        });
                        this.on("addedfile", function (file) {
                            $('#{{$id}}').removeClass('done-upload');
                            NProgress.start();
                        });
                        this.on("complete", function (file) {
                            NProgress.done();
                        });
                        this.on("error", function (file) {
                            file.status = Dropzone.QUEUED;
                            notie.alert(3, 'Sorry, try upload file again', 5);
                        });
                        this.on("success", function (file, res) {
                            //file.status = Dropzone.QUEUED;
                            $('#{{$id}}').addClass('done-upload');
                            $('#{{$id}} i').removeClass('fa-upload').addClass('fa-check');
                            //laravel.alert('');
                            //console.log('upload success...');
                            @if(isset($is_img))
                            $('#img-thumb-{{$id}}').attr('src', res.url);
                            @endif
                            $('input[name="' + $('#{{$id}}').data('input') + '"]').val(res.filename);
                        });
                    }
                });
                $('#upload-submit-{{$id}}').on('click', function (e) {
                    e.preventDefault();
                    //trigger file upload select
                    $("#{{$id}}").trigger('click');
                });
            });
        </script>
@endpush
@endif
</div>
