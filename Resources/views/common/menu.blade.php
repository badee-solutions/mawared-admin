<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 25 ماي، 2016 م
 * Time: 4:02 م
 */
?>
@foreach($items as $item)
    <li @lm-attrs($item) @lm-endattrs>
        @if($item->link)
            <a @lm-attrs($item->link) @if($item->isActive) class="active" @endif @if($item->hasChildren()) data-toggle="collapse" data-target="#{{$item->id}}"
               href="javascript:void(0);" @else href="{!! $item->url() !!}" @endif  @lm-endattrs >
                @if(isset($item->attributes['icon']))
                    <i class="{{$item->attributes['icon']}} mr-10"></i>
                @endif

                {!! $item->title !!}

                @if($item->hasChildren() || isset($item->attributes['count']))
                    <span class="pull-right">
                        @if(isset($item->attributes['count']) && $item->attributes['count'])
                            <span class="label label-success mr-10">{!! showbg($item->attributes['count']) !!} </span>
                            @if($item->isActive)
                                {{ $getuser->makeNoteRead($item->attributes['read'])}}
                            @endif
                        @endif
                        @if($item->hasChildren())
                            <i class="fa fa-fw fa-angle-down"></i>
                        @endif
                    </span>
                @endif

            </a>
        @else
            {!! $item->title !!}
        @endif
        @if($item->hasChildren())
            <ul class="collapse collapse-level-1" id="{{$item->id}}">
                @include('admin::common.menu', array('items' => $item->children()))
            </ul>
        @endif
    </li>
    @if($item->divider)
        <li {!! Lavary\Menu\Builder::attributes($item->divider) !!}></li>
    @endif
@endforeach
