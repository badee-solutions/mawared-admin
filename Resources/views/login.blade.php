@extends('admin::layouts.full-width')
@section('content')
    <div class="table-struct full-width full-height">
        <div class="table-cell vertical-align-middle">
            <div class="auth-form  ml-auto mr-auto no-float">
                <div class="panel panel-default card-view mb-0">
                    <div class="panel-heading">
                        <div class="pull-left">
                            <h6 class="panel-title txt-dark">{{ trans('user.Sign In') }}</h6>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="panel-wrapper collapse in">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-sm-12 col-xs-12">
                                    <div class="form-wrap">
                                        {!! BootForm::open()->class('ajax')->action(route('admin.login'))->method('post') !!}
                                            <div class="form-group">
                                                <label class="control-label mb-10"
                                                       for="exampleInputEmail_2">{{ trans('user.Email address') }}</label>
                                                <div class="input-group">
                                                    <input type="email" name="email" class="form-control" required=""
                                                           id="exampleInputEmail_2">
                                                    <div class="input-group-addon"><i class="icon-envelope-open"></i>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label mb-10"
                                                       for="exampleInputpwd_2">{{ trans('user.Password') }}</label>
                                                <div class="input-group">
                                                    <input type="password" name="password" class="form-control" required=""
                                                           id="exampleInputpwd_2">
                                                    <div class="input-group-addon"><i class="icon-lock"></i>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <!-- todo link forget password -->
                                                <a class="capitalize-font txt-danger block pt-5 pull-right"
                                                   href="#">{{ trans('user.forgot password') }}</a>
                                                <div class="clearfix"></div>
                                            </div>
                                            <div class="form-group">
                                                <button type="submit"
                                                        class="btn btn-success btn-block">{{ trans('user.sign in') }}</button>
                                            </div>
                                        {!! BootForm::close() !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection