<div class="table-wrap">
    <div class="clearfix"></div>
    <table class="table table-hover display dt-responsive nowrap" id="{{ $id }}" data-create="{{ $createUrl }}">
        <thead>
        <tr> @foreach ($table->getColumnsForView() as $column)
                <th>{{ $column['title'] }}</th>  @endforeach </tr>
        </thead>
        @if(isset($footer) && $footer)
            <tfoot>
            <tr> @foreach ($table->getColumnsForView() as $column)
                    <th></th>  @endforeach </tr>
            </tfoot>
        @endif
    </table>
</div>


@push('scripts')
    <script type="application/javascript">
        $(document).ready(function () {
            if (!$.fn.dataTable.isDataTable('#{{ $id }}')) {
                var table = $('#{{ $id }}').DataTable({
                    dom: 'Bfrtip',
                    oLanguage: {
                        sUrl: '{{ \App::isLocale('ar') ? asset('control/dist/js/Arabic.json') : '' }}',
                    },
                    processing: true,
                    serverSide: true,
                    bFilter: false,
                    order: [[0, 'desc']],
                    buttons: [@if($createUrl !== '#')'create', @endif 'reset', 'reload'],
                    sFilterInput: "form-control serach-datatable",
                    classes: {"sFilterInput": "form-control serach-datatable"},
                    ajax: {
                        "url": "{{ $ajaxUrl }}",
                        "type": "GET",
                        "data": function (data) {
                            for (var i = 0, len = data.columns.length; i < len; i++) {
                                if (!data.columns[i].search.value) delete data.columns[i].search;
                                if (data.columns[i].searchable === true) delete data.columns[i].searchable;
                                if (data.columns[i].orderable === true) delete data.columns[i].orderable;
                                if (data.columns[i].data === data.columns[i].name) delete data.columns[i].name;
                            }
                            delete data.search.regex;
                        },
                        "error" : laravel.ajax.errorHandler
                    },
                    columns: {!! json_encode($table->getColumnsForView()) !!},
                    initComplete: {!! view(config('admin.datatable.initComplete'))->render() !!},
                    scrollX: true,
                    fixedColumns: true,
                }).on('draw', function () {
                    $('body > .dropdown').remove();
                });
            }
        });
    </script>
@endpush