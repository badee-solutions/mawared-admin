@extends('admin::layouts.admin')
@section('content')

    <!-- Row -->
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default card-view">
                <div class="panel-wrapper collapse in">
                    <div class="panel-body">
                        <div class="table-wrap">
                            <div class="clearfix"></div>
                            {!! $dataTable->table([
                            'class' => 'table table-hover display dt-responsive nowrap',
                            'id' => (isset($dataTable_id) ? $dataTable_id :'dataTableBuilder'),
                            'data-create' => $action
                            ],(isset($tbfooter)) ? $tbfooter : false ) !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /Row -->
@endsection

@push('scripts')
    <script type="application/javascript">
        jQuery(document).ready(function ($) {
            $('#{{ (isset($dataTable_id) ? $dataTable_id :'dataTableBuilder') }}').DataTable().on('draw', function () {
                $('body > .dropdown').remove();
            });
        });
    </script>
    {!! $dataTable->scripts() !!}
@endpush
