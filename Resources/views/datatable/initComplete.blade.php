{{--<script>--}}
    function () {
        this.api().columns().every(function (i) {
            var column = this,settings = this.settings().init().columns[i];
            if (settings.filter_options !== undefined) {
                var arr = settings.filter_options;
                var input = document.createElement("select");
                input.classList.add("form-control");
                var sel = $(input).appendTo($(column.footer()).empty());
                sel.append($("<option>").attr('value', '').text('{{ trans('admin::crud.filter_by') }} '+column.title()));
                for (var k in arr){
                    sel.append($("<option>").attr('value', k).text(arr[k]));
                }
            } else if(settings.searchable) {
                var input = document.createElement("input");
                $(input).addClass('form-control');
                $(input).attr('placeholder', '{{ trans('admin::crud.search_by') }} ' + column.title());
                $(input).appendTo($(column.footer()).empty());
            }

            $(input).on('change', function () {
                var val = $.fn.dataTable.util.escapeRegex($(this).val());
                column.search(val ? val : '', false, false).draw();
            });

        });
    }

{{--</script>--}}