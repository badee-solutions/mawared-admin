@extends('admin::layouts.admin')

@section('content')


    @include('admin::base.start-row',['panel_title' => trans('admin::common.summary.users')])

    @foreach($User->chunk(4) as $users)
        <div class="row">
            @foreach($users as $user)
                <div class="col-lg-3">
                    <div class="sm-data-box bg-pink">
                        <div class="row ma-0">
                            <div class="col-xs-5 text-center pa-0 icon-wrap-left">
                                <i class="icon-people txt-light"></i>
                            </div>
                            <div class="col-xs-7 text-center data-wrap-right">
                                <h6 class="txt-light">{{ $user->name }}</h6>
                                <span class="txt-light counter counter-anim">{{ $user->users_count }}</span>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    @endforeach
    @include('admin::base.end-row')

@stop
