<?php
/**
 * Created by PhpStorm.
 * User: salah
 * Date: 25 ماي، 2016 م
 * Time: 11:36 ص
 */
?>
@include('admin::common.head',['class' => '','assets' => ['admin']])
<div class="wrapper pa-0">
    <!-- Main Content -->
    <div class="page-wrapper pa-0 ma-0">
        <div class="container-fluid">
            @yield('content')
        </div>
    </div>
</div>

@include('admin::common.footerjs',['assets' => ['admin']])