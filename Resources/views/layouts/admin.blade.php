<?php
/**
 * Created by PhpStorm.
 * User: salah
 * Date: 25 ماي، 2016 م
 * Time: 11:36 ص
 */
?>
@include('admin::common.head',['class' => '','assets' => ['admin']])
<div class="wrapper">
@include('admin::common.navbar')

@include('admin::common.sidebar')

<!-- Main Content -->
    <div class="page-wrapper">
        <div class="container-fluid">

            <!-- Title -->
            <div class="row heading-bg no-m bg-green">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <h5 class="txt-light">{{ isset($title) ? $title : trans('admin::common.dashboard') }}</h5>
                </div>
            </div>
            <!-- /Title -->
            <div class="row heading-bg bg-red no-min-height errors no-p">
                {{--<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12"></div>--}}
            </div>

            @yield('content')
        </div>
    </div>
</div>

@include('admin::common.footerjs',['assets' => ['admin']])