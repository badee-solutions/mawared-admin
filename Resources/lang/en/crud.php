<?php

/*
 * Copyright (c) 2017 Salah Alkhwlani <yemenifree@yandex.com>
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

return [
    'Add New' => 'Add New',
    'created successfully' => 'Successfully added',
    'deleted successfully' => 'Successfully deleted',
    'edit' => 'Edit',
    'list' => 'View List',
    'updated successfully' => 'successfully updated',
    'view' => 'View',
    'filter_by' => 'Filter By',
    'search_by' => 'Search By',
];
