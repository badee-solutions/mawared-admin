<?php

/*
 * Copyright (c) 2017 Salah Alkhwlani <yemenifree@yandex.com>
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

return [
  //==================================== Translations ====================================//
  'email' => 'Email',
  'mobile' => 'Mobile',
  'password_confirm' => 'Confirm Password',
  'sex' => 'Sex',
  'sex_type' => 'Sex Type',
  'type' => 'Type User',
  'active' => 'Status',
  'group' => 'Group',
  'help' => [
          'password_change' => 'Leave it blank if you do not need to change it',
      ],
  'name' => 'Username',
  'password' => 'Password',
  'profile photo' => 'Profile Photo',
  'users' => 'Users',
];
