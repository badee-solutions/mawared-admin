<?php

/*
 * Copyright (c) 2017 Salah Alkhwlani <yemenifree@yandex.com>
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

return [
    '404' => [
        'body' => 'Sorry, the requested page does not exist',
        'extend' => 'You may have made a mistake in typing the URL of the site or page you are looking for no longer exists',
        'title' => 'Content not currently available',
    ],
];
