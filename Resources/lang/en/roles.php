<?php

/*
 * Copyright (c) 2017 Salah Alkhwlani <yemenifree@yandex.com>
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

return [
    //==================================== Translations ====================================//
    'help' => [
            'is_admin' => 'This option must be enabled in order to activate adding users to the group from the site management list',
        ],
    'is_admin' => 'Administrative Group',
    'name' => 'Group name',
    'permissions' => 'Permissions',
    'roles' => 'Groups',
    'users_count' => 'Count users',
];
