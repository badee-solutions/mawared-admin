<?php

/*
 * Copyright (c) 2017 Salah Alkhwlani <yemenifree@yandex.com>
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

return [
    'roles' => 'Permissions',
    'access' => 'Login admin panel',
    'settings' => 'Edit Settings',
    'admins' => 'Control Admins',
    'full-admin' => 'General Manager (all Permissions)',
];
