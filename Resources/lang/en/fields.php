<?php

/*
 * Copyright (c) 2017 Salah Alkhwlani <yemenifree@yandex.com>
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

return [
    'depdrop_parents' => 'Parent Field',
    'selected' => 'Selected Value',
    'mobile' => 'Mobile',
];
