<?php

/*
 * Copyright (c) 2017 Salah Alkhwlani <yemenifree@yandex.com>
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

return [
    //==================================== Translations ====================================//
    'control panel' => 'Control panel',
    'error_login' => 'Sorry, the login data is incorrect',
    'error_wait_active' => 'Your account is suspend or pending active by the administration',
];
