<?php

/*
 * Copyright (c) 2017 Salah Alkhwlani <yemenifree@yandex.com>
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

return [
    //==================================== Translations ====================================//
    'action' => 'Action',
    'back to home' => 'Back to Home',
    'dashboard' => 'DashBoard',
    'setting' => 'Settings',
    'summary' => [
        'users' => 'User statistics',
    ],
    '-- select one --' => '-- Please choose --',
    'active_status' => [
        'active' => 'Active',
        'wait' => 'Wait Active',
    ],
    'administrators' => 'Administrators',
    'admins' => 'Administrators',
    'city' => 'city',
    'groups' => 'groups',
    'n/a' => 'Unknown',
    'save' => 'save',
    'cancel' => 'Cancel',
    'sex' => [
        'female' => 'female',
        'male' => 'male',
    ],
    'sex_type' => [
        'both' => 'both',
        'female' => 'female',
        'male' => 'male',
    ],
    'system' => 'settings system',
];
