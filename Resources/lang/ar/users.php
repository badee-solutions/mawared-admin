<?php

/*
 * Copyright (c) 2017 Salah Alkhwlani <yemenifree@yandex.com>
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

return [
  //==================================== Translations ====================================//
  'email' => 'البريد الالكتروني',
  'mobile' => 'الجوال',
  'password_confirm' => 'تاكيد كلمة السر',
  'sex' => 'الجنس',
  'sex_type' => 'الجنس المطلوب',
  'type' => 'نوع المشترك',
  'active' => 'الحالة',
  'group' => 'المجموعة',
  'help' => [
          'password_change' => 'اتركها فارغه في حالة عدم الحاجة لتغييرها',
      ],
  'name' => 'اسم المستخدم',
  'password' => 'كلمة السر',
  'profile photo' => 'صورة ملف العضو',
  'users' => 'المستخدمين',
];
