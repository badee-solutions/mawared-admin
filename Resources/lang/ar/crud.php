<?php

/*
 * Copyright (c) 2017 Salah Alkhwlani <yemenifree@yandex.com>
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

return [
    //============================== New strings to translate ==============================//
    // Defined in file /Volumes/yemenifree/work/htodocs/tazur/Modules/Admin/Base/BaseCRUD.php
    'view' => 'استعراض',
    //==================================== Translations ====================================//
    'Add New' => 'اضافة جديد',
    'created successfully' => 'تم الاضافة بنجاح',
    'deleted successfully' => 'تم حذف المحتوى بنجاح',
    'edit' => 'تحرير',
    'list' => 'استعراض',
    'updated successfully' => 'تم تحديث المحتوى بنجاح',
    'filter_by' => 'فلتره حسب',
    'search_by' => 'بحث في',
];
