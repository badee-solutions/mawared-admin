<?php

/*
 * Copyright (c) 2017 Salah Alkhwlani <yemenifree@yandex.com>
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

return [
    'roles' => 'التحكم بالصلاحيات',
    'access' => 'دخول لوحه التحكم',
    'settings' => 'تعديل الاعدادات',
    'admins' => 'التحكم بمدراء الموقع',
    'full-admin' => ' مدير عام ( كافه الصلاحيات)',
];
