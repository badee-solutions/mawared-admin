<?php

/*
 * Copyright (c) 2017 Salah Alkhwlani <yemenifree@yandex.com>
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

return [
  //==================================== Translations ====================================//
  'control panel' => 'لوحة التحكم',
  'error_login' => 'عفوا بيانات الدخول غير صحيحة',
  'error_wait_active' => 'حسابك موقوف او بانتظار الموافقة من قبل الادارة',
];
