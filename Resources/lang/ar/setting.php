<?php

/*
 * Copyright (c) 2017 Salah Alkhwlani <yemenifree@yandex.com>
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

return [
  //============================== New strings to translate ==============================//
  // Defined in file /Volumes/yemenifree/work/htodocs/tazur/Modules/Admin/Http/Controllers/SettingController.php
  'update' => 'تم تحديث الاعدادات بنجاح',
];
