<?php

/*
 * Copyright (c) 2017 Salah Alkhwlani <yemenifree@yandex.com>
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

return [
    //==================================== Translations ====================================//
    'help' => [
            'is_admin' => 'يجب تفعيل هذا الخيار من اجل تفعيل اضافة مستخدمين للمجموعة من قائمة ادارة الموقع',
        ],
    'is_admin' => 'مجموعة ادارية',
    'name' => 'اسم المجموعة',
    'permissions' => 'الصلاحيات',
    'roles' => 'المجموعات',
    'users_count' => 'عدد المستخدمين',
];
