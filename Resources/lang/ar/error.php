<?php

/*
 * Copyright (c) 2017 Salah Alkhwlani <yemenifree@yandex.com>
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

return [
    '404' => [
        'body' => 'المعذره ، الصفحة المطلوبة غير موجود',
        'extend' => 'قد تكون أخطأت في كتابة عنوان الموقع أو الصفحة التي تبحث عنها لم تعد موجودة',
        'title' => 'المحتوى غير متوفر حاليا',
    ],
];
