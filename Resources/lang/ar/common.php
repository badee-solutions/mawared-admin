<?php

/*
 * Copyright (c) 2017 Salah Alkhwlani <yemenifree@yandex.com>
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

return [
    //==================================== Translations ====================================//
    'action' => 'اجراء',
    'back to home' => 'العودة للرئيسية',
    'dashboard' => 'نظرة عامة',
    'setting' => 'الاعدادات',
    'summary' => [
        'users' => 'احصائيات المستخدمين',
    ],
    '-- select one --' => '-- يرجى الاختيار --',
    'active_status' => [
        'active' => 'مقبول',
        'wait' => 'في انتظار الموافقة',
    ],
    'administrators' => 'ادارة الموقع',
    'admins' => 'ادارة الموقع',
    'city' => 'المدينة',
    'groups' => 'المجموعات',
    'n/a' => 'غير معروف',
    'save' => 'حفظ',
    'cancel' => 'الغاء',
    'sex' => [
        'female' => 'انثى',
        'male' => 'ذكر',
    ],
    'sex_type' => [
        'both' => 'كلاهما',
        'female' => 'اناث',
        'male' => 'ذكور',
    ],
    'system' => 'اعدادات النظام',
];
