<?php

/*
 * Copyright (c) 2017 Salah Alkhwlani <yemenifree@yandex.com>
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

if (!\function_exists('UrlLang')) {
    /**
     * @param $url
     * @param null $lang
     *
     * @return false|string
     */
    function UrlLang($url, $lang = null)
    {
        return \LaravelLocalization::getLocalizedURL($lang, URL::to($url));
    }
}
