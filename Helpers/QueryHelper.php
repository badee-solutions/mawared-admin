<?php

/*
 * Copyright (c) 2017 Salah Alkhwlani <yemenifree@yandex.com>
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

if (!\function_exists('escape_like')) {
    /**
     * @param $string
     *
     * @return mixed
     */
    function escape_like($string)
    {
        $search = ['%', '_'];
        $replace = ['\%', '\_'];

        return \str_replace($search, $replace, $string);
    }
}
