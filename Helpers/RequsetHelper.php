<?php

/*
 * Copyright (c) 2017 Salah Alkhwlani <yemenifree@yandex.com>
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

if (!\function_exists('RequestInfo')) {
    /**
     * @return array
     */
    function RequestInfo()
    {
        list($controller, $method) = \explode('@', \Route::currentRouteAction());

        return ['c' => $controller, 'm' => $method];
    }
}

if (!\function_exists('ActiveClassMenu')) {
    /**
     * @param mixed $url
     *
     * @return string
     */
    function ActiveClassMenu($url)
    {
        return (\Request::fullUrlIs($url)) ? 'active' : '';
    }
}

if (!\function_exists('UploadFromInput')) {
    /**
     * @param $input
     * @param $path
     *
     * @return string
     */
    function UploadFromInput($input, $path = null)
    {
        $upload = request()->file($input)->storePublicly($path, ['disk' => 'public']);

        if ($upload) {
            return Storage::url($upload);
        }

        return '';
    }
}
