<?php

/*
 * Copyright (c) 2017 Salah Alkhwlani <yemenifree@yandex.com>
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

if (!\function_exists('SettingOption')) {
    /**
     * @param $key
     * @param bool|string $default
     * @param bool $lang
     *
     * @return string
     */
    function SettingOption($key, $default = null, $lang = false)
    {
        list($group) = \explode('.', $key);

        $option = \trim(substr($key, strlen($group)), '.');

        $setting = Setting::get($group);

        if (empty($option)) {
            return $setting;
        }

        if (!\is_array($setting)) {
            return $default;
        }

        $option .= $lang ? '.' . App::getLocale() : '';

        return \Illuminate\Support\Arr::get($setting, $option, $default);
    }
}

if (!\function_exists('public_url')) {
    /**
     * @param string $url
     *
     * @return false|string
     */
    function public_url($url = '')
    {
        return asset('public/' . $url);
    }
}

if (!\function_exists('ddd')) {
    function ddd(...$args)
    {
        http_response_code(500);
        call_user_func_array('dd', $args);
    }
}