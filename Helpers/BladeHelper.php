<?php

/*
 * Copyright (c) 2017 Salah Alkhwlani <yemenifree@yandex.com>
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

if (!\function_exists('createPlainMatcher')) {
    /**
     * Create a plain Blade matcher.
     *
     * @param string $function
     *
     * @return string
     */
    function createPlainMatcher($function)
    {
        return '/(?<!\w)(\s*)@' . $function . '(\s*)/';
    }
}

if (!\function_exists('showbg')) {
    function showbg($count)
    {
        if (!$count) {
            return '';
        }

        return '<span class="bg_item">' . $count . '</span>';
    }
}
