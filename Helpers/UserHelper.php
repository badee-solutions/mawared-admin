<?php

/*
 * Copyright (c) 2017 Salah Alkhwlani <yemenifree@yandex.com>
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

if (!\function_exists('HashPassword')) {
    /**
     * @param $password
     *
     * @return string
     */
    function HashPassword($password)
    {
        return \Hash::make($password);
    }
}

if (!\function_exists('CanAdminAccess')) {
    /**
     * check if account is login and not admin.
     *
     * @param $permissions
     *
     * @return array
     */
    function CanAdminAccess($permissions)
    {
        return hasAdminAccess($permissions);
    }
}

if (!\function_exists('hasAdminAccess')) {
    /**
     * check if account is login and not admin.
     *
     * @param $permissions
     *
     * @return array
     */
    function hasAdminAccess($permissions)
    {
        return \array_merge(array_wrap($permissions), ['full-admin']);
    }
}
