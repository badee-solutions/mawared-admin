<?php

/*
 * Copyright (c) 2017 Salah Alkhwlani <yemenifree@yandex.com>
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Modules\Admin\Transformers;

use Illuminate\Database\Eloquent\Model;
use League\Fractal\TransformerAbstract;

/**
 * {@inheritdoc}
 */
class NormalTransformer extends TransformerAbstract
{
    protected static $hasFakeIds = false;

    /**
     * transform basic model to id & name.
     *
     * @param $model
     *
     * @return array
     */
    public function transform($model)
    {
        return [
            'id' => $this->getKey($model),
            'name' => $model instanceof Model ? $model->name : $model['name'],
        ];
    }

    public function getKey($model)
    {
        if (!$model instanceof Model) {
            return $model['id'];
        }

        if (self::$hasFakeIds || \method_exists($model, 'getRouteKey')) {
            self::$hasFakeIds = true;

            return $model->getRouteKey();
        }

        return $model->getKey();
    }
}
