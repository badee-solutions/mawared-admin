<?php

namespace Modules\Admin\Transformers;

use Kris\LaravelFormBuilder\Fields\FormField;
use League\Fractal\TransformerAbstract;

class FormTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @param FormField $form
     *
     * @return array
     */
    public function transform(FormField $form)
    {
        return [
            'name' => $form->getName(),
            'type' => $form->getType(),
            'label' => $form->getOption('label'),
            'value' => $form->getOption('value'),
            'default_value' => $form->getOption('default_value'),
            'error_messages' => $form->getOption('error_messages'),
            'empty_value' => $form->getOption('empty_value'),
            'attr' => $form->getOption('attr'),
            'selected' => $form->getOption('selected'),
            'choices' => $form->getOption('choices'),
        ];
    }
}