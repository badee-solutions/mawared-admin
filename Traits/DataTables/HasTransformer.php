<?php

namespace Modules\Admin\Traits\DataTables;

use Yajra\DataTables\Services\DataTable;

/**
 * Trait HasTransformer
 *
 * @package Modules\Admin\Traits\DataTables
 * @property string $transformer
 * @mixin DataTable
 */
trait HasTransformer
{
    public function bootHasTransformer()
    {
        // if dattable doesn't has transfomer return early.
        if (!$this->hasTransformer()) {

            // try fetch transfomer from config.
            if ($this->getPrefixModule()) {
                $this->transformer = config($this->getPrefixModule() . '.datatable.transformer', false);
            }

            // if still transfomer missing return early.
            if (!$this->hasTransformer()) {
                return;
            }
        }

        // register callback functions.
        $this->customFunctionsTableBuilder[] = '__setTransfomerClosure';
    }

    /**
     * check if current dattable has transformer.
     *
     * @return bool
     */
    protected function hasTransformer()
    {
        return !empty($this->transformer);
    }

    /**
     * set transfomer for current datatable.
     */
    protected function __setTransfomerClosure()
    {
        $this->builder_table->setTransformer($this->getTransformer());
    }

    /**
     * get transformer class name.
     *
     * @return null|string
     */
    protected function getTransformer()
    {
        return new $this->transformer($this);
    }
}