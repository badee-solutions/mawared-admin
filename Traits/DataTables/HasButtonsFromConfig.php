<?php

/*
 * Copyright (c) 2017 Salah Alkhwlani <yemenifree@yandex.com>
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Modules\Admin\Traits\DataTables;

use Modules\Admin\Base\BaseDataTables;

/**
 * Trait HasButtonsFromConfig.
 *
 * @mixin BaseDataTables
 */
trait HasButtonsFromConfig
{
    public function bootHasButtonsFromConfig()
    {
        $this->buttons = config(($this->module ?? $this->link) . '.crud.data-table.buttons', ['create', 'reload', 'reset']);
    }
}
