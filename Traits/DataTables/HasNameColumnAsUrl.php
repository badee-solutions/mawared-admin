<?php

/*
 * Copyright (c) 2017 Salah Alkhwlani <yemenifree@yandex.com>
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Modules\Admin\Traits\DataTables;

use InvalidArgumentException;

/**
 * Trait HasNameColumnAsUrl.
 *
 * @mixin HasColumnHelper
 */
trait HasNameColumnAsUrl
{
    public function bootHasNameColumnAsUrl()
    {
        if (!\method_exists(self::class, 'bootHasColumnHelper')) {
            throw new InvalidArgumentException('This trait depend of HasColumnHelper trait');
        }
        $this->customFunctionsTableBuilder[] = '__addNameColumn';
    }

    protected function __addNameColumn()
    {
        $this->addRawColumn('name');
    }

    protected function editColumnName($data)
    {
        return '<a href="' . route('admin.' . $this->getPrefixRoute() . '.show', $data) . '">' . $data->name . '</a>';
    }
}
