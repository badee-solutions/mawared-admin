<?php

/*
 * Copyright (c) 2017 Salah Alkhwlani <yemenifree@yandex.com>
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Modules\Admin\Traits\DataTables;

use Modules\Admin\Base\BaseDataTables;

/**
 * Trait HasTranslationsColumns.
 *
 * @property array translation
 * @mixin BaseDataTables
 */
trait HasTranslationsColumns
{
    public function bootHasTranslationsColumns()
    {
        if (!\method_exists(self::class, 'bootHasColumnHelper')) {
            throw new \InvalidArgumentException('This trait depend of HasColumnHelper trait');
        }
        $this->customFunctionsTableBuilder[] = '__addTranslationsColumns';
    }

    protected function __addTranslationsColumns()
    {
        collect($this->getTranslationsColumns())->each(function ($item) {
            // add edit column
            $this->builder_table->editColumn($item, function ($data) use ($item) {
                return $this->getTranslationColumnsValue($data, $item);
            });

            // add filter column
            $this->builder_table->filterColumn($item, function ($query, $keyword) use ($item) {
                if (!str_contains($item, '.')) {
                    return $query->whereTranslation($item, '=', $keyword)->orWhere($item, 'like', '%' . escape_like($keyword) . '%');
                }

                $partsColumns = explode('.', $item);
                $relation = implode('.', array_slice($partsColumns, 0, -1));
                $column = array_pop($partsColumns);

                return $query->whereHas($relation, function ($query) use ($keyword, $column) {
                    return $query->whereTranslation($column, '=', $keyword)->orWhere($column, 'like', '%' . escape_like($keyword) . '%');
                });
            });
        });
    }

    /**
     * get translations columns.
     *
     * @return array|void
     */
    protected function getTranslationsColumns(): array
    {
        return \property_exists($this, 'translation') ? $this->translation : $this->getTranslatableFromModel();
    }

    /**
     * return translatable fields.
     *
     * @return array
     */
    protected function getTranslatableFromModel(): array
    {
        if (!$this->hasModel()) {
            return [];
        }

        return \method_exists($this->getModel(), 'getTranslatableAttributes') ? $this->getModel()->getTranslatableAttributes() : [];
    }

    /**
     * get value of translatable column.
     *
     * @param $data
     * @param $name
     *
     * @return mixed
     */
    protected function getTranslationColumnsValue($data, $name)
    {
        return data_get($data, $name);
    }
}
