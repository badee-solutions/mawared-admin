<?php

/*
 * Copyright (c) 2017 Salah Alkhwlani <yemenifree@yandex.com>
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Modules\Admin\Traits\DataTables;

use Modules\Admin\Base\BaseDataTables;

/**
 * Trait HasScopeHelper.
 *
 * @property array scopes
 * @mixin BaseDataTables
 */
trait HasScopeHelper
{
    protected function hasScope($scopeClass)
    {
        foreach ($this->scopes as $scope) {
            if ($scope instanceof $scopeClass) {
                return true;
            }
        }

        return false;
    }
}
