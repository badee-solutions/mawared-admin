<?php

namespace Modules\Admin\Traits\DataTables;

trait HasMinifiedAjax
{
    protected $ajax = [];

    /**
     * Minify ajax url generated when using get request
     * by deleting unnecessary url params.
     *
     * @param string $url
     * @param string $script
     *
     * @return array
     */
    public function minifiedAjax($url = '', $script = null)
    {
        $this->ajax = [];

        $this->ajax['url'] = $url;
        $this->ajax['type'] = 'GET';
        $this->ajax['data'] = 'function(data) {
            for (var i = 0, len = data.columns.length; i < len; i++) {
                if (!data.columns[i].search.value) delete data.columns[i].search;
                if (data.columns[i].searchable === true) delete data.columns[i].searchable;
                if (data.columns[i].orderable === true) delete data.columns[i].orderable;
                if (data.columns[i].data === data.columns[i].name) delete data.columns[i].name;
            }
            delete data.search.regex;';

        if ($script) {
            $this->ajax['data'] .= $script;
        }

        $this->ajax['data'] .= '}';

        $this->ajax['error'] = 'function (xhr, error, thrown) { laravel.ajax.errorHandler(xhr) }';

        return $this->ajax;
    }
}