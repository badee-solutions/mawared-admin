<?php

/*
 * Copyright (c) 2017 Salah Alkhwlani <yemenifree@yandex.com>
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Modules\Admin\Traits\DataTables;

use Modules\Admin\Base\BaseDataTables;

/**
 * Trait HasActionsFromConfig.
 *
 * @mixin BaseDataTables
 */
trait HasActionsFromConfig
{
    protected $except_actions = [];

    public function bootHasActionsFromConfig()
    {
        $this->except_actions = config(($this->module ?? $this->link) . '.crud.except', []);
    }

    /**
     * check if action disable and not except from crud config.
     *
     * @param $action
     *
     * @return bool
     */
    protected function isActionDisable($action)
    {
        return !$this->isActionEnable($action);
    }

    /**
     * check if action enable and not except from crud config.
     *
     * @param $action
     *
     * @return bool
     */
    protected function isActionEnable($action)
    {
        return !in_array($action, $this->except_actions, true);
    }
}
