<?php

/*
 * Copyright (c) 2017 Salah Alkhwlani <yemenifree@yandex.com>
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Modules\Admin\Traits\DataTables;

use Illuminate\Support\Str;
use Modules\Admin\Base\BaseDataTables;
use Modules\Admin\Traits\Eloquent\HasEnums;
use function call_user_func;
use function method_exists;
use function strtolower;

/**
 * Trait HasActionHelper.
 *
 * @mixin BaseDataTables
 * @property array $hasAjaxEditCrud
 */
trait HasActionHelper
{
    use HasActionsFromConfig;

    protected $action = [];

    protected function getDefaultCurdAction($data, $hideShow = false, $hideEdit = false, $hideRemove = false)
    {
        if (!$hideShow && $this->isActionEnable('show')) {
            $this->action[] = ['url' => route('admin.' . $this->getPrefixRoute() . '.show', $data), 'name' => trans('common.show'), 'icon' => 'flaticon-interface-1'];
        }

        if (!$hideEdit && $this->isActionEnable('edit')) {
            $this->action[] = $this->getEditCrudLinkAttr($data);
        }

        if (!$hideRemove && $this->isActionEnable('destroy')) {
            $this->action[] = ['url' => route('admin.' . $this->getPrefixRoute() . '.destroy', $data), 'name' => trans('common.delete'), 'icon' => 'flaticon-close', 'class' => 'ajax', 'type' => 'DELETE'];
        }
    }

    /**
     * get edit attr for crud link.
     *
     * @return array
     */
    protected function getEditCrudLinkAttr($data)
    {
        $options = [];

        if ($this->hasAjaxEditCrud ?? false) {
            $options = [
                'class'    => 'ajax-modal',
                'cssclass' => 'wid',
                'title'    => trans('common.edit')
            ];
        }

        return array_merge(['url' => route('admin.' . $this->getPrefixRoute() . '.edit', $data), 'icon' => 'flaticon-edit', 'name' => trans('common.edit')], $options);
    }

    protected function getChangeEnumsAction($data, $enums = 'status', $link = null)
    {
        $link = null === $link ? $this->getPrefixRoute() : $link;

        /**
         * @var HasEnums
         */
        $data->getAllEnumsAction($enums)->each(function ($value, $name) use (&$action, $data, $enums, $link) {

            // check if there are custom method to build action info.
            $method_name = Str::camel('Get_' . strtolower($name) . 'Action');
            if (method_exists($this, $method_name)) {
                $this->action[] = call_user_func([$this, $method_name], $data, $value, $name);

                return;
            }

            $this->action[] = ['icon' => 'flaticon-refresh', 'url' => route('admin.' . $link . '.change-' . $enums, ['id' => $data, $enums => $value]), 'class' => 'ajax', 'name' => $data->getEnumsActionText($name)];
        });
    }
}
