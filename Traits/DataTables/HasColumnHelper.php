<?php

/*
 * Copyright (c) 2017 Salah Alkhwlani <yemenifree@yandex.com>
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Modules\Admin\Traits\DataTables;

/**
 * Trait HasColumnHelper.
 */
trait HasColumnHelper
{
    public function bootHasColumnHelper()
    {
        $this->customFunctionsTableBuilder[] = '__callBackFunctionColumns';
    }

    protected function __callBackFunctionColumns()
    {
        collect($this->getColumns())->filter(function ($item, $name) {
            return \method_exists($this, $this->getFormatCallbackColumnsFunction($name));
        })->each(function ($item, $name) {
            // todo:: improve call callback
            $this->builder_table->editColumn($name, function ($data) use ($name) {
                return $this->{$this->getFormatCallbackColumnsFunction($name)}($data);
            });
        });
    }

    protected function getFormatCallbackColumnsFunction($name)
    {
        return 'editColumn' . studly_case($this->replaceCharColumnsName($name));
    }

    protected function replaceCharColumnsName($name)
    {
        return \str_replace(['.'], '-', $name);
    }

    protected function addRawColumn($name)
    {
        $this->rawColumns[] = $name;

        return $this;
    }

    protected function addEscapeColumn($name)
    {
        $this->escapeColumns[] = $name;

        return $this;
    }
}
