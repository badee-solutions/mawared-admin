<?php

/*
 * Copyright (c) 2017 Salah Alkhwlani <yemenifree@yandex.com>
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Modules\Admin\Traits\Widgets;

trait HasMultiView
{
    /**
     * get current view path.
     *
     * @return string
     */
    private function getCurrentView(): string
    {
        return data_get($this->getViews(), $this->config['view'] ?? $this->getDefaultView(), $this->getDefaultView());
    }

    /**
     * get list views.
     *
     * @return array
     */
    private function getViews(): array
    {
        return \property_exists($this, 'views') ? $this->views : [];
    }

    /**
     * get default view.
     *
     * @return string
     */
    private function getDefaultView(): string
    {
        return \property_exists($this, 'default_view') ? $this->default_view : '';
    }
}
