<?php

/*
 * Copyright (c) 2017 Salah Alkhwlani <yemenifree@yandex.com>
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Modules\Admin\Traits\Helper;

use Illuminate\Support\Collection;

trait HasCallbackEvents
{
    protected $callback_events = [];

    /**
     * add function to call before create.
     *
     * @param $event
     * @param \Closure|string $callback
     *
     * @return self
     */
    public function addCallbackEvents($event, $callback): self
    {
        $this->callback_events[$event][] = $callback;

        return $this;
    }

    /**
     * @param $event
     * @param array ...$arg
     */
    protected function callEvent($event, ...$arg)
    {
        $this->getCallbackEvents()->collect($event, [])->each(function ($callback) use ($arg) {
            if ($callback instanceof \Closure) {
                $callback(...$arg);
            }

            $this->$callback(...$arg);
        });
    }

    /**
     * @return Collection
     */
    public function getCallbackEvents(): Collection
    {
        return $this->callback_events instanceof Collection ? $this->callback_events : collect($this->callback_events);
    }
}
