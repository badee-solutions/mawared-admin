<?php

namespace Modules\Admin\Traits\Helper;


trait HasWhenHelper
{
    /**
     * Apply the callback's changes if the given "value" is true.
     *
     * @param  mixed $value
     * @param  callable $callback
     * @param  callable $default
     *
     * @return mixed
     */
    public function when($value, $callback, $default = null)
    {
        if ($value) {
            return $callback($this, $value) ?: $this;
        } elseif ($default) {
            return $default($this, $value) ?: $this;
        }

        return $this;
    }
}