<?php

namespace Modules\Admin\Traits\Helper;

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

trait HasDecodeFakeId
{
    /**
     * decode fake id or return not found http exception if fake id not valid.
     *
     * @param $value
     *
     * @throws NotFoundHttpException
     *
     * @return int
     */
    private function decodeFakeId($value)
    {
        if (!is_numeric($value) || empty($value) || is_null($value)) {
            return $value;
        }

        try {
            $value = app()->make('fakeid')->decode($value);
        } catch (\InvalidArgumentException $e) {
            throw config('app.debug') ? $e : new NotFoundHttpException("id not correct " . $value);
        }

        return $value;
    }

    /**
     * decode fake id or return not found http exception if fake id not valid.
     *
     * @param $value
     *
     * @throws NotFoundHttpException
     *
     * @return int
     */
    private function encodeFakeId($value)
    {
        if (!is_numeric($value) || empty(trim($value)) || is_null($value)) {
            return $value;
        }

        return app()->make('fakeid')->encode($value);
    }
}