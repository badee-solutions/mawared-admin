<?php

/*
 * Copyright (c) 2017 Salah Alkhwlani <yemenifree@yandex.com>
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Modules\Admin\Traits\Helper;

trait HasBooTraits
{
    /**
     * Boot all of the bootable traits on the model.
     */
    protected function bootTraits()
    {
        foreach (class_uses_recursive($this) as $trait) {
            if (\method_exists($this, $method = 'boot' . class_basename($trait))) {
                $this->{$method}();
            }
        }
    }
}
