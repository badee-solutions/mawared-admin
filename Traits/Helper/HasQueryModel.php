<?php

/*
 * Copyright (c) 2017 Salah Alkhwlani <yemenifree@yandex.com>
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Modules\Admin\Traits\Helper;

use Illuminate\Database\Eloquent\Model;

trait HasQueryModel
{
    /**
     * @property  \Prettus\Repository\Eloquent\BaseRepository repository
     */
    protected $repository;
    /**
     * @var string
     */
    protected $model;
    /**
     * @var \Illuminate\Database\Query\Builder|\Illuminate\Database\Eloquent\Builder
     */
    protected $query;

    /**
     * @return \Illuminate\Database\Query\Builder|\Illuminate\Database\Eloquent\Builder
     */
    protected function getQuery()
    {
        return $this->query ?? $this->query = $this->getModel()->newQuery();
    }

    /**
     * @param \Illuminate\Database\Query\Builder|\Illuminate\Database\Eloquent\Builder $query
     *
     * @return HasQueryModel
     */
    public function setQuery($query): self
    {
        $this->query = $query;

        return $this;
    }

    /**
     * @param bool $init
     *
     * @return \Illuminate\Database\Query\Builder|\Illuminate\Database\Eloquent\Builder|Model|string
     */
    protected function getModel($init = true)
    {
        if (!$this->hasModel()) {
            throw new \InvalidArgumentException(\get_class($this) . ' required set model first.');
        }

        return $init == false ? $this->model : new $this->model();
    }

    /**
     * @param string $model
     *
     * @return HasQueryModel
     */
    public function setModel(string $model): self
    {
        $this->model = $model;

        return $this;
    }

    /**
     * Check if class has model.
     *
     * @return bool
     */
    protected function hasModel(): bool
    {
        return !empty($this->model);
    }

    /**
     * get row via repository or model.
     *
     * @param $id
     *
     * @return \Illuminate\Database\Eloquent\Collection|Model|mixed
     */
    protected function findItem($id)
    {
        return (!$this->hasRepository()) ? $this->getModel()->findorFail($this->getId($id)) : $this->getRepository()->find($this->getId($id));
    }

    /**
     * check if has repository.
     *
     * @return bool
     */
    public function hasRepository(): bool
    {
        return !empty($this->repository);
    }

    /**
     * @return \Prettus\Repository\Eloquent\BaseRepository
     */
    public function getRepository()
    {
        if (!$this->hasRepository()) {
            throw new \InvalidArgumentException(\get_class($this) . ' required set repository first.');
        }

        return $this->repository;
    }

    /**
     * @param mixed $repository
     *
     * @return HasQueryModel
     */
    public function setRepository($repository)
    {
        $this->repository = $repository;

        return $this;
    }
}
