<?php

/*
 * Copyright (c) 2017 Salah Alkhwlani <yemenifree@yandex.com>
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Modules\Admin\Traits\Helper;

use Illuminate\Container\Container;
use Illuminate\Support\Traits\Macroable;
use ReflectionClass;
use ReflectionException;
use ReflectionMethod;
use Sofa\Eloquence\Mutator\InvalidCallableException;

trait HasAdvanceCallBack
{
    use Macroable;

    /**
     * Mutate value using provided methods.
     *
     * @param mixed        $value
     * @param string|array $callables
     *
     * @throws \LogicException
     *
     * @return mixed
     */
    public function callback($value, $callables)
    {
        if (!\is_array($callables)) {
            $callables = \explode('|', $callables);
        }

        foreach ($callables as $callable) {
            list($callable, $args) = $this->parse(\trim($callable));

            $value = \call_user_func_array($callable, \array_merge([$value], $args));
        }

        return $value;
    }

    /**
     * Parse provided mutator functions.
     *
     * @param string $callable
     *
     * @throws \Sofa\Eloquence\Mutator\InvalidCallableException
     *
     * @return array
     */
    protected function parse($callable)
    {
        list($callable, $args) = $this->parseArgs($callable);

        if ($this->isFacade($callable)) {
            $callable = $this->parseFacade($callable);
        } elseif ($this->isClassMethod($callable)) {
            $callable = $this->parseClassMethod($callable);
        } elseif ($this->isMutatorMethod($callable)) {
            $callable = [$this, $callable];
        } elseif (!\function_exists($callable)) {
            throw new InvalidCallableException("Function [{$callable}] not found.");
        }

        return [$callable, $args];
    }

    /**
     * Split provided string into callable and arguments.
     *
     * @param string $callable
     *
     * @return array
     */
    protected function parseArgs($callable)
    {
        $args = [];

        if (\strpos($callable, ':') !== false) {
            list($callable, $argsString) = \explode(':', $callable);

            $args = \explode(',', $argsString);
        }

        return [$callable, $args];
    }

    /**
     * Determine whether callable is a app facade.
     *
     * @param string $callable
     *
     * @return bool
     */
    protected function isFacade($callable)
    {
        return \strpos($callable, '@@') !== false;
    }

    /**
     * Determine whether callable is a app facade.
     *
     * @param string $userCallable
     *
     * @return bool
     */
    protected function parseFacade($userCallable)
    {
        // todo implement facade callback
        list($facade, $method) = \explode('@@', $userCallable);

        try {
            $class = app()->make($facade);

            // todo check if method exist
            //$method = new ReflectionMethod($class,$method);
        } catch (ReflectionException $e) {
            throw new InvalidCallableException($e->getMessage());
        }

        return [$facade, $method];
        //$class = new ReflectionClass($facade);

        //app();
        dd(Container::getInstance()->make($facade));
        $this->getInstanceMethod($facade, $method);

        return app($facade)->{$method}();
    }

    /**
     * Get instance callable.
     *
     * @param \ReflectionMethod $method
     * @param ReflectionClass   $class
     *
     * @throws \Sofa\Eloquence\Mutator\InvalidCallableException
     *
     * @return callable
     */
    protected function getInstanceMethod(ReflectionClass $class, ReflectionMethod $method)
    {
        if (!$method->isPublic()) {
            throw new InvalidCallableException("Instance method [{$class}@{$method->getName()}] is not public.");
        }

        if (!$this->canInstantiate($class)) {
            throw new InvalidCallableException("Can't instantiate class [{$class->getName()}].");
        }

        return [$class->newInstance(), $method->getName()];
    }

    /**
     * Determine whether instance can be instantiated.
     *
     * @param \ReflectionClass $class
     *
     * @return bool
     */
    protected function canInstantiate(ReflectionClass $class)
    {
        if (!$class->isInstantiable()) {
            return false;
        }

        $constructor = $class->getConstructor();

        return null === $constructor || 0 === $constructor->getNumberOfRequiredParameters();
    }

    /**
     * Determine whether callable is a class method.
     *
     * @param string $callable
     *
     * @return bool
     */
    protected function isClassMethod($callable)
    {
        return \strpos($callable, '@') !== false;
    }

    /**
     * Extract and validate class method.
     *
     * @param string $userCallable
     *
     * @throws \Sofa\Eloquence\Mutator\InvalidCallableException
     *
     * @return callable
     */
    protected function parseClassMethod($userCallable)
    {
        list($class) = \explode('@', $userCallable);

        $callable = \str_replace('@', '::', $userCallable);

        try {
            $method = new ReflectionMethod($callable);

            $class = new ReflectionClass($class);
        } catch (ReflectionException $e) {
            throw new InvalidCallableException($e->getMessage());
        }

        return ($method->isStatic()) ? $callable : $this->getInstanceMethod($class, $method);
    }

    /**
     * Determine whether callable is available on this instance.
     *
     * @param string $callable
     *
     * @return bool
     */
    protected function isMutatorMethod($callable)
    {
        return \method_exists($this, $callable) || static::hasMacro($callable);
    }
}
