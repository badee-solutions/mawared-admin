<?php

/*
 * Copyright (c) 2017 Salah Alkhwlani <yemenifree@yandex.com>
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Modules\Admin\Traits\Helper;

use Illuminate\Http\Request;

trait HasRequest
{
    /** @var array|string|Request */
    protected $request;

    /**
     * @param null  $method
     * @param array $arg
     *
     * @return array|string|Request
     */
    public function getRequest($method = null, ...$arg)
    {
        if (!isset($this->request)) {
            $this->request = request();
        }

        if (null === $method) {
            return $this->request;
        }

        return $this->request->$method(...$arg);
    }

    /**
     * @param mixed $request
     *
     * @return HasRequest
     */
    public function setRequest($request): self
    {
        $this->request = $request;

        return $this;
    }
}
