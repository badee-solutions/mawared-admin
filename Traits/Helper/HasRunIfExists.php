<?php

/*
 * Copyright (c) 2017 Salah Alkhwlani <yemenifree@yandex.com>
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Modules\Admin\Traits\Helper;

trait HasRunIfExists
{
    /**
     * run internal method if exists.
     *
     * @param $method
     * @param $default
     * @param array ...$arg
     *
     * @return bool|mixed
     */
    protected function runIfMethodExists($method, $default = false, ...$arg)
    {
        if (!\method_exists($this, $method)) {
            // if default is callback function.
            if ($default instanceof \Closure) {
                return $default(...$arg);
            }

            return $default;
        }

        return $this->$method(...$arg);
    }
}
