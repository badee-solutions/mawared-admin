<?php

/*
 * Copyright (c) 2017 Salah Alkhwlani <yemenifree@yandex.com>
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Modules\Admin\Traits;

trait HasSelectDropList
{
    protected static $key_select_list = 'id';
    protected static $value_select_list = 'name';

    public function scopeSelectList($query)
    {
        return $query->get()->pluck(self::$value_select_list, self::$key_select_list)->all();
    }
}
