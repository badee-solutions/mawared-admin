<?php

/*
 * Copyright (c) 2017 Salah Alkhwlani <yemenifree@yandex.com>
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Modules\Admin\Traits\Eloquent;

use Illuminate\Database\Eloquent\Builder;

/**
 * Trait HasActiveStatus.
 *
 * @mixin \Eloquent
 */
trait HasActiveStatus
{
    /**
     * return sex type with label.
     *
     * @return array
     */
    public static function getActiveStatus()
    {
        return [
            0 => trans('admin::common.active_status.wait'),
            1 => trans('admin::common.active_status.active'),
        ];
    }

    /**
     * filter by active status of record.
     *
     * @param $query Builder
     * @param $status
     *
     * @return mixed
     */
    public function scopeByActive($query, $status)
    {
        $status = \is_array($status) ? $status : [$status];

        return $query->whereIn('active', $status);
    }

    /**
     * filter active status.
     *
     * @param $query Builder
     *
     * @return mixed
     */
    public function scopeActive($query)
    {
        return $query->where('active', '>', 0);
    }
}
