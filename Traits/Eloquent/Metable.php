<?php

namespace Modules\Admin\Traits\Eloquent;

use Sofa\Eloquence\Builder;
use Sofa\Eloquence\Metable as BaseMetable;
use Sofa\Hookable\Contracts\ArgumentBag;

trait Metable
{
    use BaseMetable;

    /**
     * Adjust meta columns for select statement.
     *
     * @param  \Sofa\Eloquence\Builder $query
     * @param  \Sofa\Hookable\Contracts\ArgumentBag $args
     *
     * @return void
     */
    protected function metaSelect(Builder $query, ArgumentBag $args)
    {
        $columns = $args->get('columns');

        foreach ($columns as $key => $column) {
            list($column, $alias) = $this->extractColumnAlias($column);

            if ($this->hasColumn($column)) {
                $select = "{$this->getTable()}.{$column}";

                if ($column !== $alias) {
                    $select .= " as {$alias}";
                }

                $columns[$key] = $select;
            } elseif (is_string($column) && $column != '*' && strpos($column, '.') === false && $this->allowsMeta($column)) {
                $table = $this->joinMeta($query, $column);

                $columns[$key] = "{$table}.meta_value as {$alias}";
            }
        }

        $args->set('columns', $columns);
    }
}