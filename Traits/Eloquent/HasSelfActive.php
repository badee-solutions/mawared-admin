<?php

/*
 * Copyright (c) 2017 Salah Alkhwlani <yemenifree@yandex.com>
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Modules\Admin\Traits\Eloquent;

/**
 * Trait HasSelfActive.
 *
 * @property bool is_active
 *
 * @mixin \Eloquent
 */
trait HasSelfActive
{
    public function activate()
    {
        $this->is_active = true;
        $this->save();
    }
}
