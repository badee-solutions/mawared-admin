<?php

/*
 * Copyright (c) 2017 Salah Alkhwlani <yemenifree@yandex.com>
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Modules\Admin\Traits\Eloquent;

/**
 * Trait HasLogo.
 *
 * @method string getFirstMediaUrl($collectionName)
 *
 * @property string default_logo
 * @mixin \Eloquent
 */
trait HasLogo
{
    /**
     * get logo.
     *
     * @return string
     */
    public function getLogoAttribute()
    {
        $logo = $this->getFirstMediaUrl('logo');

        return !$logo ? asset($this->getDefaultLogo()) : $logo;
    }

    /**
     * Get the array of attribute mutators.
     *
     * @return string
     */
    public function getDefaultLogo()
    {
        return (\property_exists($this, 'default_logo')) ? $this->default_logo : 'front/images/favicon.png';
    }
}
