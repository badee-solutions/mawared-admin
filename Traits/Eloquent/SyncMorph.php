<?php

/*
 * Copyright (c) 2017 Salah Alkhwlani <yemenifree@yandex.com>
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Modules\Admin\Traits\Eloquent;

use Modules\Admin\Support\Arrays;

/**
 * Trait SyncMorph.
 *
 * @mixin \Eloquent
 */
trait SyncMorph
{
    /**
     * @param $relationship
     * @param string $column
     * @param array  $values
     */
    public function sync($relationship, $column = 'id', array $values = [])
    {
        if (\count($values) == 0 && request()->has($relationship)) {
            foreach (request()->get($relationship) as $info) {
                $values[] = $this->{$relationship}()->firstOrCreate($info)->id;
            }
        }

        $new_values = \array_filter($values);
        $old_values = $this->$relationship->pluck($column)->all();

        // Delete removed values, if any
        if ($deleted = Arrays::keysDeleted($new_values, $old_values)) {
            $this->{$relationship}()->whereIn('id', $deleted)->delete();
        }

        // TODO :: clean up

//        // Create new values, if any
//        if ($created = Arrays::keysCreated($new_values, $old_values)) {
//            foreach ($created as $id) {
//                $new[] = $this->$relationship()->getModel()->newInstance([
//                    $column => $new_values[$id],
//                ]);
//            }
//            $this->$relationship()->saveMany($new);
//        }
//
//        // Update changed values, if any
//        if ($updated = Arrays::keysUpdated($new_values, $old_values)) {
//            foreach ($updated as $id) {
//                $this->$relationship()->find($id)->update([
//                    $column => $new_values[$id],
//                ]);
//            }
//        }
    }
}
