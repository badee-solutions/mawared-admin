<?php

/*
 * Copyright (c) 2017 Salah Alkhwlani <yemenifree@yandex.com>
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Modules\Admin\Traits\Eloquent;

use Ghanem\Rating\Models\Rating;
use Illuminate\Database\Eloquent\Model;

/**
 * Trait Rateable.
 *
 *
 * @mixin \Eloquent
 */
trait Rateable
{
    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany|\Illuminate\Database\Query\Builder
     */
    public function averageRating()
    {
        return $this->ratings()
            ->selectRaw('ratingable_id,AVG(rating) as averageRating')
            ->groupby('ratingable_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function ratings()
    {
        return $this->morphMany(Rating::class, 'ratingable');
    }

    /**
     * @return int
     */
    public function getAverageRatingAttribute()
    {
        // if relation is not loaded already, let's do it first
        if (!\array_key_exists('averageRating', $this->relations)) {
            $this->load('averageRating');
        }

        $related = $this->getRelation('averageRating');

        return (int) ($related->pluck('averageRating')->first());
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function sumRating()
    {
        return $this->ratings()
            ->selectRaw('SUM(rating) as sumRating')
            ->pluck('sumRating');
    }

    /**
     * @param $max
     *
     * @return int
     */
    public function ratingPercent($max = 5)
    {
        $ratings = $this->ratings();
        $quantity = $ratings->count();
        $total = $ratings->selectRaw('SUM(rating) as total')->pluck('total');

        return ($quantity * $max) > 0 ? $total / (($quantity * $max) / 100) : 0;
    }

    /**
     * @param $data
     * @param Model      $author
     * @param Model|null $parent
     *
     * @return static
     */
    public function rating($data, Model $author, Model $parent = null)
    {
        return (new Rating())->createRating($this, $data, $author);
    }

    /**
     * @param $id
     * @param $data
     * @param Model|null $parent
     *
     * @return mixed
     */
    public function updateRating($id, $data, Model $parent = null)
    {
        return (new Rating())->updateRating($id, $data);
    }

    /**
     * @param $id
     *
     * @return mixed
     */
    public function deleteRating($id)
    {
        return (new Rating())->deleteRating($id);
    }
}
