<?php

/*
 * Copyright (c) 2017 Salah Alkhwlani <yemenifree@yandex.com>
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Modules\Admin\Traits\Eloquent;

use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use InvalidArgumentException;
use ReflectionClass;

/**
 * Trait HasEnums.
 *
 * @mixin \Eloquent
 */
trait HasEnums
{
    /**
     * Check if the given value is valid within the given group.
     *
     * @param mixed       $value
     * @param null|string $group
     *
     * @return bool
     */
    public static function isValidEnumValue($value, $group = null)
    {
//        if ($value < 0) {
//            throw new InvalidArgumentException('An enumerator value must be greater than or equal to 0.');
//        }

        $constants = static::enums($group);

        return \in_array($value, $constants);
    }

    /**
     * The array of enumerators of a given group.
     *
     * @param null|string $group
     *
     * @return array
     */
    public static function enums($group = null)
    {
        $constants = (new ReflectionClass(\get_called_class()))->getConstants();

        if ($group) {
            $group = static::toEnumKey($group);

            return \array_filter($constants, function ($key) use ($group) {
                return \strpos($key, $group . '_') === 0;
            }, ARRAY_FILTER_USE_KEY);
        }

        return $constants;
    }

    /**
     * Convert string to an Enumerator key value.
     *
     * @param string      $key
     * @param null|string $group
     *
     * @return string
     */
    public static function toEnumKey($key, $group = null)
    {
        if (!\ctype_upper(\str_replace('_', '', $key))) {
            $key = \preg_replace('/\s+/u', '', $key);
            $key = \strtoupper(\preg_replace('/(.)(?=[A-Z])/u', '$1_', $key));
        }

        if ($group) {
            return static::toEnumKey((string) $group) . '_' . $key;
        }

        return $key;
    }

    /**
     * Get action button text.
     *
     * @param $enumKey | $name
     * @param string $group
     *
     * @return \Illuminate\Contracts\Translation\Translator|string
     */
    public static function getEnumsActionText($enumKey, $group = 'status')
    {
        if (\is_numeric($enumKey)) { // if pass value get name of value from group
            $enumKey = static::toEnumName($enumKey, $group);
        }

        return trans(static::getNamespaceTransEnum() . 'action.' . $enumKey);
    }

    /**
     * Convert key value to an Enumerator name string.
     *
     * @param $value
     * @param null|string $group
     *
     * @return string
     */
    public static function toEnumName($value, $group = null)
    {
//        if ($value < 0) {
//            throw new InvalidArgumentException('An enumerator value must be greater than or equal to 0.');
//        }

        $constants = \array_flip(static::enums($group));

        return Arr::get($constants, $value, 'UNKNOWN');
    }

    /**
     * get namespace of translation for current enums.
     *
     * @return string
     */
    protected static function getNamespaceTransEnum()
    {
        return !\property_exists(self::class, 'namespaceTransEnum') ? '' : static::$namespaceTransEnum . '.';
    }

    /**
     * return list of enums as key(id) => value.
     *
     * @param string $enums
     *
     * @return array
     */
    public static function getEnumsAsKeyValue($enums = 'status')
    {
        return collect(self::enums($enums))->mapWithKeys(function ($item, $key) {
            return [$item => self::getEnumsLabelText($key)];
        })->toArray();
    }

    /**
     * get enums label for value vis key value or name.
     *
     * @param $enumKey
     * @param string $group
     *
     * @return array|\Illuminate\Contracts\Translation\Translator|null|string
     */
    public static function getEnumsLabelText($enumKey, $group = 'status')
    {
        if (\is_numeric($enumKey)) { // if pass value get name of value from group
            $enumKey = static::toEnumName($enumKey, $group);
        }

        return trans(static::getNamespaceTransEnum() . 'label.' . $enumKey);
    }

    /**
     * Check if the class is the given Enumerator.
     *
     * @param string      $key
     * @param null|string $group
     *
     * @return bool
     */
    public function isEnum($key, $group = null)
    {
        $enumKey = $this->toEnumKey($key, $group);

        if (!static::isValidEnumKey($enumKey)) {
            throw new InvalidArgumentException("The key {$key} is not a valid Enumerator key" . ($group ? " for group {$group}." : '.'));
        }

        $getProp = function ($group) {
            return \lcfirst(\str_replace(' ', '', \ucwords(\str_replace(['-', '_'], ' ', \strtolower($group)))));
        };

        if (!$group) {
            $parts = \explode('_', $enumKey);

            do {
                \array_pop($parts);

                $group = $this->toEnumKey(\implode('_', $parts));
                $prop = $getProp($group);
            } while (!\array_key_exists($prop, $this->getAttributes()) && \count($parts));
        } else {
            $group = $this->toEnumKey($group);
            $prop = $getProp($group);
        }

        if (!\array_key_exists($prop, $this->getAttributes()) && (\property_exists($this, 'allowedMeta') && !\in_array($prop, $this->getAllowedMeta()))) {
            return false;
        }

        return $this->$prop == static::enums($group)[$enumKey];
    }

    /**
     * Check if the given key exists.
     *
     * @param mixed $key
     *
     * @return bool
     */
    public static function isValidEnumKey($key)
    {
        return \array_key_exists($key, static::enums());
    }

    /**
     * get list of action they available for current enums..
     *
     * @param string $type
     *
     * @return static
     */
    public function getAllEnumsAction($type = 'status')
    {
        return collect($this->enums($type))->filter(function ($item, $enumKey) use ($type) {
            return $this->canDoEnumsAction($type, $enumKey);
        });
    }

    /**
     * Check if allow to change status(enums) from current status(enums).
     *
     * @param $type
     * @param $enumKey
     *
     * @return bool
     */
    public function canDoEnumsAction($type, $enumKey)
    {
        if (!isset($this->{$type})) {
            throw new InvalidArgumentException(\sprintf('"invalid action type [%s] in enums group [%s].', $type, $enumKey));
        }
        $customMethod = true;

//        if (!property_exists($this, $type)) {
//            return false;
//        }

        // if pass value of enums get name of value
        if (\is_numeric($enumKey)) {
            $enumKey = $this->toEnumName($enumKey, $type);
        }

        // callback for enums has custom method conditions
        $overrideMethod = Str::camel('allowEnums_' . \strtolower($enumKey));
        if (\method_exists($this, $overrideMethod)) {
            $customMethod = $this->$overrideMethod($enumKey);
        }

        return $customMethod && \in_array($this->{$type}, static::checkEnumsConditions($enumKey));
    }

    public static function checkEnumsConditions($enumKey)
    {
        if (!static::isValidEnumKey($enumKey)) {
            throw new InvalidArgumentException("The key {$enumKey} is not a valid Enumerator key");
        }

        return Arr::get(self::getEnumsConditions(), $enumKey, []);
    }

    public static function getEnumsConditions()
    {
        return \property_exists(self::class, 'enumsConditions') ? self::$enumsConditions : [];
    }
}
