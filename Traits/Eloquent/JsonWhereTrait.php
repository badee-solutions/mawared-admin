<?php

/*
 * Copyright (c) 2017 Salah Alkhwlani <yemenifree@yandex.com>
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Modules\Admin\Traits\Eloquent;

use Illuminate\Database\Eloquent\Builder;

/**
 * Trait JsonWhereTrait.
 *
 * @mixin \Eloquent
 */
trait JsonWhereTrait
{
    /**
     * @param $query Builder
     * @param $column
     * @param string $operator
     * @param $value
     *
     * @return mixed
     *
     * @internal param $field
     */
    public function scopeJsonWhere($query, $column, $operator, $value)
    {
        if ($this->BuildInJson()) { // if mysql v >= 5.7 use build in json syntax $column->$ar
            return $query->where($column, $operator, $value);
        }

        $column = \explode('->', $column);

        // todo :: add fallback
        if (\count($column) !== 2) {
            return $query;
        }
        // todo :: improve like & add support others operator
        $value = \str_replace('\\', '\\\\\\\\', \json_encode($value)); // fix UNICODE code
        $value = \str_replace('%\\\\\\\\', '%\\', $value); // remove first \\\\ after % if value UNICODE

        $field = escape_like($column[1]);
        $column_name = escape_like($column[0]);

        //  LIKE '%"{$field}":"%{$value}%"%'
        return $query->whereRaw("$column_name LIKE '%\"{$field}\":{$value}%'");
    }

    /**
     * check if has build in json column.
     *
     * @return bool
     */
    public function BuildInJson()
    {
        // todo :: think maybe add as config better of hint database
        $pdo = \DB::connection()->getPdo();
        $version = $pdo->query('select version()')->fetchColumn();
        (float) $version = \mb_substr($version, 0, 6);

        // mysql >= 5.6 has fulltext index support
        if ($version >= '5.7') {
            return true;
        }

        return false;
    }
}
