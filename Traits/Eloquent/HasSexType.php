<?php

/*
 * Copyright (c) 2017 Salah Alkhwlani <yemenifree@yandex.com>
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Modules\Admin\Traits;

use Illuminate\Database\Eloquent\Builder;

/**
 * Trait HasSexType.
 *
 * @mixin \Eloquent
 */
trait HasSexType
{
    /**
     * return sex type with label.
     *
     * @return array
     */
    public static function getSexType()
    {
        return [
            0 => trans('admin::common.sex_type.male'),
            1 => trans('admin::common.sex_type.female'),
            2 => trans('admin::common.sex_type.both'),
        ];
    }

    /**
     * return sex with label.
     *
     * @return array
     */
    public static function getSex()
    {
        return [
            0 => trans('admin::common.sex.male'),
            1 => trans('admin::common.sex.female'),
        ];
    }

    /**
     * filter by sex of record.
     *
     * @param $query Builder
     * @param $sex
     *
     * @return mixed
     */
    public function scopeBySex($query, $sex)
    {
        $sex = \is_array($sex) ? $sex : [$sex];

        return $query->whereIn('sex', $sex);
    }

    /**
     * filter by sex type of record.
     *
     * @param $query Builder
     * @param $sex
     *
     * @return mixed
     */
    public function scopeBySexType($query, $sex)
    {
        $sex = \is_array($sex) ? $sex : [$sex];

        return $query->whereIn('sex_type', $sex);
    }
}
