<?php

/*
 * Copyright (c) 2017 Salah Alkhwlani <yemenifree@yandex.com>
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Modules\Admin\Traits\Eloquent;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Localization;

/**
 * Trait HasTranslations.
 *
 * @mixin \Eloquent
 * @property array $translatable
 */
trait HasTranslations
{
    use JsonWhereTrait, \Spatie\Translatable\HasTranslations;

    /**
     * @param $query Builder|Model
     * @param $column
     * @param $operator
     * @param $value
     *
     * @return mixed
     */
    public function scopeWhereTranslation($query, $column, $operator, $value)
    {
        list($column, $locale) = \array_pad(\explode('->', $column, 2), 2, null);

        $locale = $locale ?? Localization::getCurrentLocale();

        return $query->jsonWhere(\implode('->', [$column, $locale]), $operator, $value);
    }

    /**
     * @param $key
     *
     * @throws \Spatie\Translatable\Exceptions\AttributeIsNotTranslatable
     *
     * @return array
     */
    public function getTranslations($key): array
    {
        $this->guardAgainstUntranslatableAttribute($key);

        $value = \json_decode($this->getAttributes()[ $key ] ?? '' ?: '{}', true);

        if (!\is_array($value) || (null === $value && !empty($this->getAttributes()[ $key ]))) { // fallback if old data still not translate
            $value = [Localization::getCurrentLocale() => $this->getAttributes()[ $key ]];
        }

        return $value;
    }

    /**
     * Set a given attribute on the model.
     *
     * @param string $key
     * @param mixed $value
     *
     * @return $this
     */
    public function setAttribute($key, $value)
    {
        // pass arrays and untranslatable attributes to the parent method
        if (!$this->isTranslatableAttribute($key) || \is_array($value)) {
            return \method_exists($this, 'setAttributeHooked') ? $this->setAttributeHooked($key, $value) : parent::setAttribute($key, $value);
        }
        // if the attribute is translatable and not already translated (=array),
        // set a translation for the current app locale
        return $this->setTranslation($key, config('app.locale'), $value);
    }
}
