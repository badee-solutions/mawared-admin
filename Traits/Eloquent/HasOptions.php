<?php

/*
 * Copyright (c) 2017 Salah Alkhwlani <yemenifree@yandex.com>
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Modules\Admin\Traits\Eloquent;

/**
 * Trait HasOptions.
 *
 *
 * @property array options
 * @property array attributes
 * @mixin \Eloquent
 */
trait HasOptions
{
    /**
     * get options via key.
     *
     * @param $key
     * @param bool $default
     *
     * @return bool
     */
    public function getOptions($key, $default = false)
    {
        if (!\is_object($this->options) || !isset($this->options->{$key})) {
            return $default;
        }

        return $this->options->{$key};
    }

    /**
     * get options from database.
     *
     * @param $value
     *
     * @return mixed
     */
    public function getOptionsAttribute($value)
    {
        return \json_decode($value);
    }

    /**
     * save options to database.
     *
     * @param $value
     */
    public function setOptionsAttribute($value)
    {
        $this->attributes['options'] = \json_encode($value);
    }
}
