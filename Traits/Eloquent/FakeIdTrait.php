<?php

/*
 * Copyright (c) 2017 Salah Alkhwlani <yemenifree@yandex.com>
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Modules\Admin\Traits\Eloquent;

use Propaganistas\LaravelFakeId\FakeIdTrait as BaseFakeIdTrait;

trait FakeIdTrait
{
    use BaseFakeIdTrait;

    public function getFakeIdAttribute($value)
    {
        return $this->getRouteKey();
    }

    /**
     * Get the value of the model's route key.
     *
     * @return mixed
     */
    public function getRouteKey()
    {
        return $this->getKey() ? app('fakeid')->encode($this->getKey()) : 0;
    }
}
