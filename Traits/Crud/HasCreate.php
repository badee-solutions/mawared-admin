<?php

/*
 * Copyright (c) 2017 Salah Alkhwlani <yemenifree@yandex.com>
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Modules\Admin\Traits\Crud;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\JsonResponse;
use Kris\LaravelFormBuilder\Form;
use View;

/**
 * @method string getActionCreate()
 * @method View renderCreate()
 * @method void|null beforeCreateRequest($request)
 * @method Form|JsonResponse beforeValidFormRequest(Form $form)
 * @method array|JsonResponse afterValidFormRequest(Form $form, array $data)
 * @method array filterCreateBeforeSave(array $data)
 * @method void|null afterCreateSave(Model $model, array $data)
 * @method void|View|JsonResponse renderStore(Model $model)
 * @method array getCreateJsonResponse(array $data)
 * @method string getCreateRedirect()
 */
trait HasCreate
{
    protected $createRules = [];
    protected $disabledCreateRedirect = false;

    /**
     *  Create form.
     *
     * @return mixed
     */
    public function create()
    {
        \Assets::group('admin')->add($this->getAssetsForm());
        $this->data['action'] = $this->runIfMethodExists('getActionCreate', route('admin.' . $this->url . '.index'));
        $this->setTitle($this->getLabel()['new']);

        if ($this->getUseForm()) {
            $this->data['form'] = $this->form($this->getUseForm(), $this->getOptionsForm(), $this->getDataForm());
        }

        $this->data['form_title'] = $this->getLabel()['new'];

        return $this->runIfMethodExists('renderCreate', function () {
            return view($this->getFormViewTemplate(), $this->data);
        });
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     *
     * @internal param Request $request
     * @internal param Ajax $ajax
     */
    protected function store()
    {
        $this->runIfMethodExists('beforeCreateRequest', null, $this->getRequest());

        if ($this->getUseForm()) {
            $input = $this->_handler_store_form();

            if ($input instanceof \Illuminate\Http\JsonResponse) {
                // if valid callback (return response object) fail
                return $input;
            }
        } else {
            $input = $this->validate($this->getRequest(), $this->getCreateRules());
        }

        $input = $this->runIfMethodExists('filterCreateBeforeSave', $input, $input);

        $model = $this->saveStoreData($input);

        $this->runIfMethodExists('afterCreateSave', null, $model, $input);

        return $this->runIfMethodExists('renderStore', function () use ($model) {
            $data = $this->runIfMethodExists('getCreateJsonResponse', []);

            if (!$this->isDisabledCreateRedirect()) {
                $data['redirect'] = $this->runIfMethodExists('getCreateRedirect', route('admin.' . $this->url . '.index'), $model);
            }

            return $this->getAjax()->setJson($data)->alert($this->getLabel()['create'], 1)->jsonResponse();
        }, $model);
    }

    /**
     * Handler store form.
     *
     * @return bool|mixed
     */
    protected function _handler_store_form()
    {
        /** @var Form $form */
        $form = $this->form($this->getUseForm(), [], $this->dataForm);

        $callbackValid = $this->runIfMethodExists('beforeValidFormRequest', true, $form);
        if ($callbackValid instanceof \Illuminate\Http\JsonResponse) {
            // if valid callback (return response object) fail
            return $callbackValid;
        }

        // It will automatically use current request, get the rules, and do the validation
        if (!$form->isValid()) {
            return $this->getAjax()->alert($form->getErrors())->customError($this->getErrorBag())->jsonResponse(422);
        }

        $input = $form->getFieldValues();

        return $this->runIfMethodExists('afterValidFormRequest', $input, $form, $input);
    }

    /**
     * Get Create Rules for direct validate.
     *
     * @return array
     */
    public function getCreateRules(): array
    {
        return $this->createRules;
    }

    /**
     * Save data via repository or model.
     *
     * @param $input
     *
     * @return mixed
     */
    protected function saveStoreData($input)
    {
        return $this->hasRepository() ? $this->getRepository()->create($input) : $this->getModel()->create($input);
    }

    /**
     * Detect if create redirect disabled.
     *
     * @return bool
     */
    public function isDisabledCreateRedirect(): bool
    {
        return $this->disabledCreateRedirect;
    }
}
