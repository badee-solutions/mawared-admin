<?php

/*
 * Copyright (c) 2017 Salah Alkhwlani <yemenifree@yandex.com>
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Modules\Admin\Traits\Crud;

use Kris\LaravelFormBuilder\FormBuilderTrait;

/**
 * @property  \Prettus\Repository\Eloquent\BaseRepository repository
 * @property string view_form namespace of view [module].
 * @property string prefix_view_form internal namespace of view[sub-module].
 * @property string use_form form class.
 * @property bool with_null if you want get all input data includes null values from form class.
 * @property bool error_bag error class name ( selector format ) to display error validations.
 * @property array dataForm data to pass to form class.
 * @property array defaultOptionForm options to pass to form class.
 *
 * @method \Kris\LaravelFormBuilder\Form form($name, array $options = [], array $data = [])
 */
trait HasFormHelper
{
    use FormBuilderTrait;

    protected $view_form;
    protected $prefix_view_form = '';
    protected $useForm = null;
    protected $with_null = true;
    protected $error_bag = '.errors';
    protected $dataForm = ['admin' => true];
    protected $defaultOptionForm = ['class' => 'ui form ajax m-form m-form--fit m-form--label-align-right', 'enctype' => 'multipart/form-data', 'method' => 'POST'];
    protected $assetsForm = ['ckeditor', 'forms'];

    /**
     * get current form class name.
     *
     * @return null|string
     */
    public function getUseForm(): string
    {
        return $this->useForm;
    }

    /**
     * @param null $useForm
     *
     * @return HasFormHelper
     */
    public function setUseForm($useForm): self
    {
        $this->useForm = $useForm;

        return $this;
    }

    /**
     * get class ( selector ) of error bag.
     *
     * @return string
     */
    public function getErrorBag(): string
    {
        return $this->error_bag;
    }

    /**
     * @return mixed
     */
    public function getViewForm(): string
    {
        return $this->view_form ?: $this->view;
    }

    /**
     * @param mixed $view_form
     *
     * @return HasFormHelper
     */
    public function setViewForm($view_form): self
    {
        $this->view_form = $view_form;

        return $this;
    }

    /**
     * @return string
     */
    public function getPrefixViewForm(): string
    {
        return $this->prefix_view_form ?: $this->prefix_view;
    }

    /**
     * @param string $prefix_view_form
     *
     * @return HasFormHelper
     */
    public function setPrefixViewForm(string $prefix_view_form): self
    {
        $this->prefix_view_form = $prefix_view_form;

        return $this;
    }

    /**
     * @return array
     */
    public function getAssetsForm(): array
    {
        return \array_unique($this->assetsForm);
    }

    /**
     * @param array $assetsForm
     *
     * @return self
     */
    public function addAssetsForm(array $assetsForm): self
    {
        $this->assetsForm = \array_merge($this->assetsForm, $assetsForm);

        return $this;
    }

    /**
     * @param array $options
     *
     * @return array
     */
    protected function getOptionsForm($options = []): array
    {
        $options = \array_merge($this->getDefaultOptionsForm(), $options);

        return \array_merge(['url' => $this->data['action']], $options);
    }

    /**
     * @return array
     */
    protected function getDefaultOptionsForm(): array
    {
        return $this->defaultOptionForm;
    }

    /**
     * add options for form.
     *
     * @param $key
     * @param $value
     *
     * @return $this
     */
    protected function addOptionsForm($key, $value): self
    {
        $this->defaultOptionForm[$key] = $value;

        return $this;
    }

    /**
     * get data of form.
     *
     * @param array $data
     *
     * @return array
     */
    protected function getDataForm($data = []): array
    {
        return \array_merge($this->dataForm, $data);
    }

    /**
     * Add data to form.
     *
     * @param $key
     * @param $value
     *
     * @return self
     */
    protected function addDataForm($key, $value): self
    {
        $this->dataForm[$key] = $value;

        return $this;
    }


    /**
     * get view template base of request type.
     *
     * @return string
     */
    protected function getFormViewTemplate()
    {
        return \Request::wantsJson() || \Request::ajax() ? 'admin::components.form' : $this->getViewForm() . '::admin' . $this->getPrefixViewForm() . '.form';
    }
}
