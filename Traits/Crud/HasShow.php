<?php

/*
 * Copyright (c) 2017 Salah Alkhwlani <yemenifree@yandex.com>
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Modules\Admin\Traits\Crud;

use Assets;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\JsonResponse;
use View;

/**
 * @method Model|View loadShowInfo(int $id)
 * @method void beforeShowView()
 * @method View|JsonResponse renderShow()
 */
trait HasShow
{
    protected $assetsShow = [];

    /**
     * Show info.
     *
     * @param $id
     *
     * @return Model
     */
    public function show($id)
    {
        Assets::group('admin')->add($this->getAssetsShow());
        $this->data['info'] = $this->runIfMethodExists('loadShowInfo', function () use ($id) {
            return $this->findItem($id);
        }, $id);
        $this->setTitle(\implode(' - ', [$this->getLabel()['view'], $this->getInfoTitle()]));

        $this->runIfMethodExists('beforeShowView', null);

        return $this->runIfMethodExists('renderShow', function () {
            return view($this->getView() . '::admin' . $this->getPrefixView() . '.show', $this->data);
        });
    }

    /**
     * @return array
     */
    public function getAssetsShow(): array
    {
        return \array_unique($this->assetsShow);
    }

    /**
     * @param array $assetsShow
     *
     * @return self
     */
    public function addAssetsShow(array $assetsShow): self
    {
        $this->assetsShow = \array_merge($this->assetsShow, $assetsShow);

        return $this;
    }
}
