<?php

/*
 * Copyright (c) 2017 Salah Alkhwlani <yemenifree@yandex.com>
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Modules\Admin\Traits\Crud;

/**
 * @method void afterDestroyModel(int $id)
 */
trait HasDestroy
{
    /**
     * @param int $id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        $item = $this->findItem($id);
        $item->delete();

        $this->runIfMethodExists('afterDestroyModel', null, $id, $item);

        return $this->runIfMethodExists('renderDestroy', function () {
            return $this->getAjax()->reloadTable($this->getDataTableHtmlId())->alert($this->getLabel()['deleted'], 1)->jsonResponse();
        }, $id, $item);
    }
}
