<?php

/*
 * Copyright (c) 2017 Salah Alkhwlani <yemenifree@yandex.com>
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Modules\Admin\Traits\Crud;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\JsonResponse;
use Kris\LaravelFormBuilder\Form;
use View;

/**
 * @method string getActionEdit(Model $model)
 * @method View renderEdit()
 * @method void|null beforeUpdateRequest(int $id)
 * @method Form|JsonResponse beforeValidFormRequest(Form $form)
 * @method array|JsonResponse afterValidFormRequest(Form $form, array $data)
 * @method array filterUpdateInputBeforeFill(Model $model, array $data)
 * @method Model filterUpdateBeforeSave(Model $model, array $data)
 * @method void|null afterUpdateSave(Model $model, array $data)
 * @method void|View|JsonResponse renderUpdate(Model $model)
 * @method array getUpdateJsonResponse(array $data)
 * @method string getUpdateRedirect()
 */
trait HasEdit
{
    /**
     * @var array
     */
    protected $updateRules = [];

    /**
     * @var bool
     */
    protected $disabledUpdateRedirect = false;

    /**
     * Edit Form.
     *
     * @param int|Model $id
     *
     * @return View
     */
    public function edit($id)
    {
        $this->runIfMethodExists('beforeEditForm', null, $id);

        \Assets::group('admin')->add($this->getAssetsForm());
        $this->data['info'] = $this->findItem($id);
        $this->data['action'] = $this->runIfMethodExists('getActionEdit', route('admin.' . $this->url . '.update', ['id' => $this->data['info']]), $this->data['info']);

        $this->runIfMethodExists('beforeEditFormInit', null, $id);

        if ($this->getUseForm()) {
            $this->data['form'] = $this->form($this->getUseForm(), $this->getOptionsForm(['method' => 'PUT', 'model' => $this->data['info']]), $this->getDataForm(['update' => true]));
        }

        $this->setTitle(\implode(' - ', [$this->getLabel()['edit'], $this->getInfoTitle()]));

        $this->data['form_title'] = $this->data['title'];

        return $this->runIfMethodExists('renderEdit', function () {
            return view($this->getFormViewTemplate(), $this->data);
        });
    }

    /**
     * @param Model|int $id
     *
     * @return JsonResponse
     */
    protected function update($id)
    {
        //Eventy::action('crud.update.before.request');

        $id = $this->getId($id);

        $this->runIfMethodExists('beforeUpdateRequest', null, $id);

        $model = $this->findItem($id);

        if ($this->getUseForm()) {
            $input = $this->_handler_update_form($model);

            if ($input instanceof JsonResponse) { // if valid callback (return response object) fail
                return $input;
            }
        } else {
            $input = $this->validate($this->getRequest(), $this->getUpdateRules());
        }

        $input = $this->runIfMethodExists('filterUpdateInputBeforeFill', $input, $model, $input);

        $model = $this->runIfMethodExists('filterUpdateBeforeSave', $model, $model, $input);

        if ($this->hasRepository()) {
            $model = $this->getRepository()->update($input, $id);
        } else {
            $model->fill($input)->save();
        }

        $this->runIfMethodExists('afterUpdateSave', null, $model, $input);

        return $this->runIfMethodExists('renderUpdate', function () use ($model) {
            $data = $this->runIfMethodExists('getUpdateJsonResponse', []);

            if (!$this->isDisabledUpdateRedirect()) {
                $data['redirect'] = $this->runIfMethodExists('getUpdateRedirect', route('admin.' . $this->url . '.index'), $model);
            }

            return $this->getAjax()->setJson($data)->alert($this->getLabel()['update'], 1)->jsonResponse();
        }, $model);
    }

    /**
     * handler update form.
     *
     * @param $model
     *
     * @return array|JsonResponse
     */
    protected function _handler_update_form($model)
    {
        /** @var Form $form */
        $form = $this->form($this->getUseForm(), ['model' => $model], \array_merge($this->dataForm, ['update' => true]));

        $callbackValid = $this->runIfMethodExists('beforeValidFormRequest', null, $form);
        if ($callbackValid instanceof JsonResponse) { // if valid callback (return response object) fail
            return $callbackValid;
        }

        if (!$form->isValid()) {
            return $this->getAjax()->alert($form->getErrors())->customError($this->error_bag)->jsonResponse(422);
        }

        $input = $form->getFieldValues($this->with_null);

        return $this->runIfMethodExists('afterValidFormRequest', $input, $form, $input);
    }

    /**
     * @return array
     */
    public function getUpdateRules(): array
    {
        return $this->updateRules;
    }

    /**
     * @return bool
     */
    public function isDisabledUpdateRedirect(): bool
    {
        return $this->disabledUpdateRedirect;
    }
}
