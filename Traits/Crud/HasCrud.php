<?php

/*
 * Copyright (c) 2017 Salah Alkhwlani <yemenifree@yandex.com>
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Modules\Admin\Traits\Crud;

use App\Services\Ajax\Ajax;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Modules\Admin\Traits\Helper\HasQueryModel;
use Modules\Admin\Traits\Helper\HasRunIfExists;

trait HasCrud
{
    use HasRunIfExists, HasQueryModel, HasFormHelper;

    protected $url;
    protected $module;
    protected $view;
    protected $prefix_view = '';
    protected $labels = [];
    protected $titleProperty = 'name';
    /**
     * @var string
     */
    protected $defaultTitle = '';

    /** @var Request */
    protected $request;
    /** @var Ajax */
    protected $ajax;

    /**
     * @return Request
     */
    public function getRequest()
    {
        return $this->request ?? $this->request = request();
    }

    /**
     * @return Ajax
     */
    public function getAjax()
    {
        return $this->ajax ?? $this->ajax = app('ajax');
    }

    /**
     * @return string
     */
    public function getPrefixView(): string
    {
        return $this->prefix_view;
    }

    /**
     * @param string $prefix_view
     *
     * @return self
     */
    public function setPrefixView(string $prefix_view): self
    {
        $this->prefix_view = $prefix_view;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getView(): string
    {
        return $this->view;
    }

    /**
     * @param string $view
     *
     * @return self
     */
    public function setView($view): self
    {
        $this->view = $view;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param mixed $url
     *
     * @return self
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getModule()
    {
        return $this->module ?? $this->link;
    }

    /**
     * @param string $module
     *
     * @return self
     */
    public function setModule($module)
    {
        $this->module = $module;

        return $this;
    }

    /**
     *  Pages title labels.
     *
     * @return array
     */
    protected function getLabel()
    {
        return \array_merge([
            'list' => trans('admin::crud.list'),
            'new' => trans('admin::crud.Add New'),
            'view' => trans('admin::crud.view'),
            'edit' => trans('admin::crud.edit'),
            'create' => trans('admin::crud.created successfully'),
            'update' => trans('admin::crud.updated successfully'),
            'deleted' => trans('admin::crud.deleted successfully'),
        ], $this->labels);
    }

    /**
     * add label.
     *
     * @param $key
     * @param $value
     *
     * @return $this
     */
    protected function addLabel($key, $value)
    {
        $this->labels[$key] = $value;

        return $this;
    }

    /**
     * Get title for row current edit or view.
     *
     * @return string
     */
    protected function getInfoTitle()
    {
        return Arr::get($this->data['info'], $this->getTitleProperty(), $this->getDefaultTitle());
    }

    /**
     * @return string
     */
    public function getTitleProperty(): string
    {
        return $this->titleProperty;
    }

    /**
     * @param string $titleProperty
     *
     * @return self
     */
    public function setTitleProperty(string $titleProperty): self
    {
        $this->titleProperty = $titleProperty;

        return $this;
    }

    /**
     * @return string
     */
    public function getDefaultTitle(): string
    {
        return $this->defaultTitle;
    }

    /**
     * @param string $defaultTitle
     *
     * @return self
     */
    public function setDefaultTitle(string $defaultTitle): self
    {
        $this->defaultTitle = $defaultTitle;

        return $this;
    }

    /**
     * get Id from item.
     *
     * @param $item
     *
     * @return int
     */
    protected function getId($item)
    {
        return ($item instanceof Model) ? $item->getKey() : $item;
    }
}
