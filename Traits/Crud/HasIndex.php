<?php

/*
 * Copyright (c) 2017 Salah Alkhwlani <yemenifree@yandex.com>
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Modules\Admin\Traits\Crud;

use Assets;
use Modules\Admin\Base\BaseDataTables;

/**
 * @property BaseDataTables $dataTable
 *
 * @mixin HasCrud
 */
trait HasIndex
{
    protected $dataTable;
    protected $dataTableHtmlId = '#dataTableBuilder';
    protected $footer = false;
    protected $assetsIndex = ['datatables'];
    protected $dataTableViewName = 'admin::datatable.show';

    /**
     * @return string
     */
    public function getDataTableHtmlId(): string
    {
        return $this->dataTableHtmlId;
    }

    /**
     * @param string $dataTableHtmlId
     *
     * @return HasIndex
     */
    public function setDataTableHtmlId(string $dataTableHtmlId): self
    {
        $this->dataTableHtmlId = $dataTableHtmlId;

        return $this;
    }

    /**
     * @param $assets
     *
     * @return HasIndex
     */
    public function addAssetsIndex($assets): self
    {
        $this->assetsIndex = \array_merge($this->assetsIndex, $assets);

        return $this;
    }

    /**
     * @return BaseDataTables
     */
    public function getDataTable(): BaseDataTables
    {
        return $this->dataTable;
    }

    /**
     * Get List row table.
     *
     * @return mixed
     */
    protected function index()
    {
        $this->runIfMethodExists('beforeIndex');

        $this->setTitle($this->getLabel()['list']);
        Assets::group('admin')->add($this->getAssetsIndex());
        $this->data['tbfooter'] = $this->isFooter();
        $this->data['dataTable_id'] = \ltrim($this->dataTableHtmlId, '#');
        $createUrl = method_exists($this, 'create') && !in_array('create', config(($this->module ?? $this->getUrl()) . '.crud.except', [])) ? route('admin.' . $this->url . '.create') : '';
        $this->data['action'] = $this->runIfMethodExists('getActionCreateForm', $createUrl, $createUrl);
        $this->data['create_label'] = $this->getLabel()['new'] ?? '';

        return $this->getDataTable()->render($this->getDataTableViewName(), $this->data);
    }

    /**
     * @return array
     */
    public function getAssetsIndex(): array
    {
        return \array_unique($this->assetsIndex);
    }

    /**
     * @return bool
     */
    public function isFooter(): bool
    {
        return $this->footer;
    }

    /**
     * @param bool $footer
     *
     * @return HasIndex
     */
    public function setFooter(bool $footer): self
    {
        $this->footer = $footer;

        return $this;
    }

    /**
     * get custome view for datatable.
     *
     * @return string
     */
    protected function getDataTableViewName()
    {
        return $this->dataTableViewName;
    }

    /**
     * set custome view for datatable.
     *
     * @param string $view
     *
     * @return $this
     */
    protected function setDataTableViewName($view)
    {
        $this->dataTableViewName = $view;

        return $this;
    }

    /**
     * set list title.
     *
     * @param $title
     *
     * @return self
     */
    protected function setListTitle($title): self
    {
        $this->labels['list'] = $title;

        return $this;
    }
}
