<?php

namespace Modules\Admin\Traits\Requests;


use Illuminate\Support\Arr;

trait HasFilterByRules
{

    /**
     * Get all of the input and files for the request.
     *
     * @param  array|mixed $keys
     *
     * @return array
     */
    public function all($keys = null)
    {
        $input = array_replace_recursive($this->input(), $this->allFiles());

        if (!$keys) {
            return $input;
        }

        $results = [];

        $keys = $keys ?? $this->rules();

        foreach (is_array($keys) ? $keys : func_get_args() as $key) {
            Arr::set($results, $key, Arr::get($input, $key));
        }

        return $results;
    }

}