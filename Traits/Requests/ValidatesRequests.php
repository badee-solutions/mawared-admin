<?php

/*
 * Copyright (c) 2017 Salah Alkhwlani <yemenifree@yandex.com>
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Modules\Admin\Traits\Requests;

use Illuminate\Foundation\Validation\ValidatesRequests as BaseValidatesRequests;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;
use Modules\Admin\Exception\ValidationAlertException;

trait ValidatesRequests
{
    use BaseValidatesRequests;

    /**
     * Validate the given request with the given rules.
     *
     * @param Request $request
     * @param array   $rules
     * @param array   $messages
     * @param array   $customAttributes
     *
     * @throws ValidationAlertException
     * @throws ValidationException
     *
     * @return array
     */
    public function validate(
        Request $request,
        array $rules,
                             array $messages = [],
        array $customAttributes = []
    ) {
        try {
            $validator = $this->getValidationFactory()
                ->make($request->all(), $rules, $messages, $customAttributes);
            $validator->validate();
        } catch (ValidationException $exception) {
            if ($this->validate_as_alert) {
                throw new ValidationAlertException($validator);
            }

            throw $exception;
        }

        return $this->extractInputFromRules($request, $rules);
    }
}
