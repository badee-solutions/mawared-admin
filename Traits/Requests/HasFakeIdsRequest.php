<?php

/*
 * Copyright (c) 2017 Salah Alkhwlani <yemenifree@yandex.com>
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Modules\Admin\Traits\Requests;

use Illuminate\Http\Request;
use Modules\Admin\Traits\Helper\HasDecodeFakeId;

/**
 * Trait HasFakeIdsRequest.
 */
trait HasFakeIdsRequest
{
    use HasDecodeFakeId;

    protected $fakeIds;

    /**
     * load middleware from controller instated of Middleware class.
     */
    protected function loadMiddleware()
    {
        $this->middleware(function ($request, $next) {
            $request = $this->decodeFakeIdsInRoute($request);

            return $next($request);
        });
    }

    /**
     * decode fake ids of parameters in route.
     *
     * @param Request $request
     *
     * @return Request
     */
    protected function decodeFakeIdsInRoute($request)
    {
        if (!$request->route()->hasParameters()) {
            return $request;
        }

        foreach ($request->route()->parametersWithoutNulls() as $key => $value) {
            $request->route()->setParameter($key, $this->transform($key, $value));
        }

        return $request;
    }

    /**
     * decode fake ids of parameters in route.
     *
     * @param Request $request
     *
     * @return Request
     */
    protected function decodeFakeIdsInRequest($request)
    {
        $this->getFakeIds()->filter(function ($item) use ($request) {
            return $request->has($item) && !empty($request->get($item));
        })->each(function ($item) use ($request) {
            $request->offsetSet($item, $this->decodeFakeId($request->get($item)));
        });

        return $request;
    }

    /**
     * get list of fake ids.
     *
     * @return \Illuminate\Support\Collection
     */
    protected function getFakeIds()
    {
        return $this->fakeIds ?? $this->fakeIds = collect($this->attributes);
    }

}
