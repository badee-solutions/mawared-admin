<?php

/*
 * Copyright (c) 2017 Salah Alkhwlani <yemenifree@yandex.com>
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Modules\Admin\Traits\Requests;

use App\Services\Ajax\Ajax;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\JsonResponse;
use Modules\Admin\Traits\Crud\HasIndex;

/**
 * Trait HasChangeStatusRequest.
 *
 * @mixin HasIndex
 */
trait HasChangeStatusRequest
{
    /**
     * Change status of model base of id & new status.
     *
     * @param $id
     * @param $status
     *
     * @return JsonResponse
     */
    public function changeStatus($id, $status)
    {
        $change = $this->getRepository()->changeStats($id, $status);

        $return = app('ajax')->alert($change['msg'], $change['status']);

        if (\property_exists($this, 'dataTable')) {
            $return->reloadTable($this->getDataTableHtmlId());
        }

        return $this->responseChangeStatus($return, $change, data_get($change, 'info', null));
    }

    /**
     * @param Ajax  $return
     * @param array $change
     * @param Model $info
     *
     * @return JsonResponse
     */
    public function responseChangeStatus($return, $change, $info = null)
    {
        return $return->jsonResponse($change['status'] == 1 ? 200 : 422);
    }
}
