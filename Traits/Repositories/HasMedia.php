<?php

/*
 * Copyright (c) 2017 Salah Alkhwlani <yemenifree@yandex.com>
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Modules\Admin\Traits\Repositories;

use Illuminate\Support\Collection;
use Modules\Admin\Base\BaseRepository;

/**
 * Trait HasMedia.
 *
 * @mixin BaseRepository
 *
 * @property array media_collection
 */
trait HasMedia
{
    /**
     * @param array $media_collection
     *
     * @return HasMedia
     */
    public function setMediaCollection(array $media_collection): self
    {
        $this->media_collection = $media_collection;

        return $this;
    }

    /**
     * boot trait.
     */
    protected function bootHasMedia()
    {
        $this->addCallbackEvents('afterCreate', 'saveMedia');
        $this->addCallbackEvents('afterUpdate', 'saveMedia');
    }

    /**
     * save media collection from request.
     *
     * @param $model
     * @param mixed $attributes
     */
    protected function saveMedia($model, $attributes)
    {
        $this->getMediaCollection()->each(function ($input_name, $media_collection) use ($model) {
            $media_collection = is_numeric($media_collection) ? $input_name : $media_collection;

            if (request()->hasFile($input_name)) {
                $model->clearMediaCollection($media_collection);
                $model->addMediaFromRequest($input_name)->toMediaCollection($media_collection);
            }
        });
    }

    /**
     * @return Collection
     */
    public function getMediaCollection(): Collection
    {
        return collect($this->media_collection ?? []);
    }
}
