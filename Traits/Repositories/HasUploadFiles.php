<?php

/*
 * Copyright (c) 2017 Salah Alkhwlani <yemenifree@yandex.com>
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Modules\Admin\Traits\Repositories;

use Modules\Admin\Base\BaseRepository;

/**
 * Trait HasUploadFiles.
 *
 * @property array upload_field
 * @mixin BaseRepository
 */
trait HasUploadFiles
{
    /**
     * @param array|string $upload_field
     *
     * @return $this
     */
    public function setUploadField($upload_field)
    {
        $this->upload_field = array_wrap($upload_field);

        return $this;
    }

    /**
     * @param $attributes
     */
    protected function uploadFiles(&$attributes)
    {
        foreach ($this->getUploadField() as $filed) {
            if (request()->hasFile($filed)) {
                $attributes[$filed] = UploadFromInput($filed);
            }
        }
    }

    /**
     * boot trait.
     */
    protected function bootHasUploadFiles()
    {
        $this->addCallbackEvents('beforeCreate', 'uploadFiles');
        $this->addCallbackEvents('beforeUpdate', 'uploadFiles');
    }

    /**
     * @return array
     */
    public function getUploadField(): array
    {
        return $this->upload_field ?? [];
    }
}
