<?php

/*
 * Copyright (c) 2017 Salah Alkhwlani <yemenifree@yandex.com>
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Modules\Admin\Traits\Repositories;

use Modules\Admin\Base\BaseRepository;
use Modules\Admin\Entities\Roles;

/**
 * Trait HasRole.
 *
 * @mixin BaseRepository
 */
trait HasRole
{

    /**
     * boot trait.
     */
    protected function bootHasRole()
    {
        $this->addCallbackEvents('afterCreate', 'updateRoleUser');
        //$this->addCallbackEvents('afterUpdate', 'uploadLogo');
    }

    /**
     * @param $model
     * @param $attributes
     */
    protected function updateRoleUser($model, $attributes)
    {
        if (!$this->rule) {
            return;
        }

        // set role of user.
        $roles = Roles::Slug($this->rule)->first()->getKey();
        $model->roles()->sync([$roles]);
    }
}
