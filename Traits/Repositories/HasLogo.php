<?php

/*
 * Copyright (c) 2017 Salah Alkhwlani <yemenifree@yandex.com>
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Modules\Admin\Traits\Repositories;

use Modules\Admin\Base\BaseRepository;

/**
 * Trait HasUploadFiles.
 *
 * @property bool hasLogo
 * @property string logo_field
 * @mixin BaseRepository
 */
trait HasLogo
{
    /**
     * @param string $logo_field
     *
     * @return HasLogo
     */
    public function setLogoField(string $logo_field): self
    {
        $this->logo_field = $logo_field;

        return $this;
    }

    /**
     * boot trait.
     */
    protected function bootHasLogo()
    {
        $this->addCallbackEvents('beforeCreate', 'uploadLogo');
        $this->addCallbackEvents('beforeUpdate', 'uploadLogo');
    }

    /**
     * @param $attributes
     */
    protected function uploadLogo(&$attributes)
    {
        // prevent fill logo via user.
        unset($attributes['logo']);

        if ($this->isHasLogo()) {
            $attributes['logo'] = UploadFromInput($this->getLogoField());
        }
    }

    /**
     * @return bool
     */
    public function isHasLogo(): bool
    {
        return $this->hasLogo && request()->hasFile($this->getLogoField());
    }

    /**
     * @return string
     */
    public function getLogoField(): string
    {
        return $this->logo_field ?? 'cover';
    }
}
