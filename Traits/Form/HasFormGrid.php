<?php

/*
 * Copyright (c) 2017 Salah Alkhwlani <yemenifree@yandex.com>
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Modules\Admin\Traits\Form;

trait HasFormGrid
{
    /**
     * Add fields as group.
     *
     * @param \Closure $callback
     * @param null     $name
     * @param string   $count
     */
    private function groupFields(\Closure $callback, $name = null, $count = 'two')
    {
        $this->startGroupFields($name, $count);
        $callback();
        $this->endGroupFields($name);
    }

    /**
     * start group fields.
     *
     * @param null   $name
     * @param string $count
     */
    private function startGroupFields($name = null, $count = 'two')
    {
        $name = null === $name ? str_random() : $name;
        $this->add('start-' . $name, 'collapse', ['wrapper' => ['class' => $count . ' fields clearfix']]);
    }

    /**
     * close group fields.
     *
     * @param null $name
     */
    private function endGroupFields($name = null)
    {
        $name = null === $name ? str_random() : $name;
        $this->add('end' . $name, 'collapse', ['type' => true]);
    }
}
