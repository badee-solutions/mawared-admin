<?php

/*
 * Copyright (c) 2017 Salah Alkhwlani <yemenifree@yandex.com>
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Modules\Admin\Traits\Form;

use Illuminate\Database\Eloquent\Model;

trait HasMultiLangFields
{
    protected $localized = true;

    /**
     * Create a new field multi-lang and add it to the form.
     *
     * @param string $name
     * @param string $type
     * @param array $options
     * @param bool $modify
     *
     * @return $this
     */
    public function addM($name, $type = 'text', array $options = [], $modify = false)
    {
        // if current form not support localized add field as normal and return early.
        if (!$this->isLocalized()) {
            return $this->add($name, $type, $options, $modify);
        }

        $this->setData('hasLang', true);

        $this->getLocales()->each(function ($item) use ($name, $type, $options, $modify) {
            $options['label'] .= ' [' . $item->native() . ']';
            $options['wrapper'] = ['data-depends' => '[{\'#lang-switch\' : {\'type\':\'equal\',\'value\': \'' . $item->key() . '\'}}]'];
            $options['rules'] = data_get($this->rules, $name);
            if ($this->getModel() && $this->getModel() instanceof Model) {
                $options['default_value'] = $this->getModel()->getTranslation($name, $item->key());
            }

            $this->add($name . '[' . $item->key() . ']', $type, $options, $modify);
        });

        return $this;
    }

    /**
     * check if current fields support localized from config or not.
     *
     * @return bool
     */
    protected function isLocalized()
    {
        return $this->localized;
    }

    /**
     * enable/disabled localized for form.
     *
     * @return bool
     */
    protected function setLocalized($boolean)
    {
        $this->localized = $boolean;

        return $this;
    }

    /**
     * get List of Locales active.
     *
     * @return \Illuminate\Support\Collection
     */
    protected function getLocales()
    {
        return \Localization::getSupportedLocales();
    }
}
