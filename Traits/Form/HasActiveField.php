<?php

/*
 * Copyright (c) 2017 Salah Alkhwlani <yemenifree@yandex.com>
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Modules\Admin\Traits\Form;

trait HasActiveField
{
    protected function appendActiveField()
    {
        $this->add('is_active', 'checkbox', [
            'label' => trans('common.is_active'),
            'attr' => [
                'class' => 'bs-switch',
                'data-on-text' => trans('common.yes'),
                'data-off-text' => trans('common.no'),
            ],
        ]);
    }
}
