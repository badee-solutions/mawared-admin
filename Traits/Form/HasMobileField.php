<?php

/*
 * Copyright (c) 2017 Salah Alkhwlani <yemenifree@yandex.com>
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Modules\Admin\Traits\Form;

use Modules\Admin\Base\BaseForm;

/**
 * Trait HasMobileField.
 *
 * @mixin BaseForm
 */
trait HasMobileField
{
    /**
     * @param array  $options
     * @param string $fieldName
     */
    protected function addMobileField($options = [], $fieldName = 'mobile')
    {
        $this->add($fieldName, 'number', \array_merge([
            'label' => trans('admin::fields.mobile'),
            'attr' => ['data-mobile' => $this->getDataModel('country_code', 'sa')],
            'rules' => 'required|numeric|country_code_required|phone:country_code,mobile',
        ], $options));
    }
}
