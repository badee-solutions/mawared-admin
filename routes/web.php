<?php

/*
 * Copyright (c) 2017 Salah Alkhwlani <yemenifree@yandex.com>
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

use App\User;

Route::group([
    'middleware' => \array_merge(['web', 'auth:' . URL::admin('login'), 'admin'], config('admin.middleware', [])),
    'prefix' => URL::admin(),
    'as' => 'admin.',
], function () {
    Route::resource('roles', config('admin.controller.roles') . '\RolesController');
    Route::fakeIdModel('user', User::class);
    Route::changeEnums('users', 'active', config('admin.controller.users') . '\UsersController');
    Route::resource('users', config('admin.controller.users') . '\UsersController');
    Route::get('setting/{module}', '\Modules\Admin\Http\Controllers\SettingController@index');
    Route::post('setting/{module}', [
        'uses' => '\Modules\Admin\Http\Controllers\SettingController@store',
        'as' => 'setting',
    ]);
});

Route::group([
    'middleware' => ['web'],
    'prefix' => URL::admin(),
    'namespace' => config('admin.controller.login'),
    'as' => 'admin.auth.',
], function () {
    Route::get('/login', 'LoginController@login')->name('login');
    Route::post('/login', 'LoginController@store')->name('do-login');
});
