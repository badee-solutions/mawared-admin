<?php

/*
 * Copyright (c) 2017 Salah Alkhwlani <yemenifree@yandex.com>
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Modules\Admin\Base;

use Modules\Admin\Traits\Helper\HasBooTraits;
use Modules\Admin\Traits\Helper\HasCallbackEvents;
use Modules\Admin\Traits\Helper\HasRunIfExists;
use Modules\Admin\Traits\Helper\HasWhenHelper;

/**
 * Class BaseRepository
 * @package Modules\Admin\Base
 * @method void afterCreate($model, array $attributes)
 * @method void beforeCreate(array $attributes)
 * @method void afterUpdate($model, array $attributes)
 * @method void beforeUpdate($model, array $attributes)
 */
abstract class BaseRepository extends \Prettus\Repository\Eloquent\BaseRepository
{
    use HasRunIfExists, HasBooTraits, HasCallbackEvents, HasWhenHelper;

    public function getModel()
    {
        return $this->model;
    }

    /**
     *  boot traits.
     */
    public function boot()
    {
        $this->bootTraits();
    }

    /**
     * get latest of rows.
     *
     * @return $this
     */
    public function latest()
    {
        $this->model = $this->model->latest();

        return $this;
    }

    /**
     * Load Count relations.
     *
     * @param array|string $relations
     *
     * @return $this
     */
    public function withCount($relations)
    {
        $this->model = $this->model->withCount($relations);

        return $this;
    }

    /**
     * Create Model (make sure filter all input before call it).
     *
     * @param array $attributes
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     *
     * @return mixed
     */
    public function create(array $attributes)
    {
        $this->callEvent('beforeCreate', $attributes);

        $this->runIfMethodExists('beforeCreate', null, $attributes);

        $attributes = $this->runIfMethodExists('filterCreateAttributes', $attributes, $attributes);

        $model = parent::create($attributes);

        if (!$model) {
            return $model;
        }

        $this->callEvent('afterCreate', $model, $attributes);

        $this->runIfMethodExists('afterCreate', null, $model, $attributes);

        return $model;
    }

    /**
     * Update Model (Make sure filter all input before call it).
     *
     * @param array $attributes
     * @param $id
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     *
     * @return mixed
     */
    public function update(array $attributes, $id)
    {
        $this->callEvent('beforeUpdate', $attributes);

        $this->runIfMethodExists('beforeUpdate', null, $attributes, $id);

        $attributes = $this->runIfMethodExists('filterUpdateAttributes', $attributes, $attributes, $id);

        $model = parent::update($attributes, $id);

        $this->callEvent('afterUpdate', $model, $attributes);

        $this->runIfMethodExists('afterUpdate', null, $model, $attributes);

        return $model;
    }


    public function __call($name, $arguments)
    {
        if (!method_exists($this, $name)) {
            if (in_array($name, $this->getScopeModel(), true)) {
                $this->model = $this->model->{$name}(...$arguments);

                return $this;
            }

            trigger_error('Call to undefined method ' . __CLASS__ . '::' . $name . '()', E_USER_ERROR);
        }

        return $this->{$name}(...$arguments);
    }

    /**
     * @return array
     */
    public function getScopeModel(): array
    {
        return $this->scopeModel ?? [];
    }
}
