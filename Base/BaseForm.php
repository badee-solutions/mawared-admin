<?php

/*
 * Copyright (c) 2017 Salah Alkhwlani <yemenifree@yandex.com>
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Modules\Admin\Base;

use Config;
use Illuminate\Support\Collection;
use Kris\LaravelFormBuilder\Fields\FormField;
use Kris\LaravelFormBuilder\Form;
use Kris\LaravelFormBuilder\FormHelper;
use Modules\Admin\Traits\Helper\HasRunIfExists;
use Validator;

class BaseForm extends Form
{
    use HasRunIfExists;

    protected $is_update = false;
    protected $rules = null;
    protected $rules_replace = null;
    protected $wrapper = 'col-md-6 form-group';
    protected $sometimesRules = [];

    public static function setUI($ui = 'bootstrap')
    {
        if ($ui == 'ui') {
            Config::set('laravel-form-builder.defaults', Config::get('laravel-form-builder.ui'));
        } else {
            Config::set('laravel-form-builder.defaults', Config::get('laravel-form-builder.bootstrap'));
        }

        app()->extend('laravel-form-helper', function ($app) {
            $configuration = Config::get('laravel-form-builder');

            return new FormHelper(app('view'), app('translator'), $configuration);
        });
    }

    /**
     * Validate the form.
     *
     * @param array $validationRules
     * @param array $messages
     *
     * @return Validator
     */
    public function validate($validationRules = [], $messages = [])
    {
        return tap(parent::validate(), function () {
            $this->getSometimesRules()->each(function ($item, $field) {
                $this->validator->sometimes($field, ...$item);
            });

            $this->runIfMethodExists('afterCreateValidate');
        });
    }

    /**
     * get list of sometimes rules.
     *
     * @return array|Collection
     */
    public function getSometimesRules()
    {
        return $this->sometimesRules instanceof Collection ? $this->sometimesRules : collect($this->sometimesRules);
    }

    /**
     * add sometimes rules.
     *
     * @param $field
     * @param $rules
     * @param $callback
     *
     * @return $this
     */
    public function addSometimesRule($field, $rules, $callback)
    {
        $this->sometimesRules[$field] = [$rules, $callback];

        return $this;
    }

    /**
     * Get model that is bind to form object.
     *
     * @param $name
     * @param bool $default
     *
     * @return mixed
     */
    public function getDataModel($name, $default = false)
    {
        if (
            // if model not ready yet
            !$this->getModel() ||
            // or if ready but value is empty
            (($value = $this->__getDataModel($name)) && empty($value))
        ) {
            if ($default instanceof \Closure) {
                return $default();
            }

            return $default;
        }

        return $value;
    }

    /**
     * @param $name
     *
     * @return mixed
     */
    protected function __getDataModel($name)
    {
        return $name instanceof \Closure ? $name() : data_get($this->getModel(), $name);
    }

    /**
     * get fields as collection.
     *
     * @return static
     */
    public function getFieldsAsCollection()
    {
        return collect($this->getFields())->reject(function ($field) {
            /** @var FormField $field */
            return in_array($field->getType(), ['collapse']);
        });
    }

    /**
     * Get validation rules for the form.
     *
     * @param array $overrideRules
     *
     * @return array
     */
    public function getRules($overrideRules = [])
    {
        $overrideRules = array_merge($overrideRules, $this->rules);

        $fieldRules = $this->formHelper->mergeFieldsRules($this->fields);

        return array_merge($fieldRules['rules'], $overrideRules);
    }

    protected function appendRules($rules = [])
    {
        if (null === $this->rules && \count($rules) > 0) {
            $this->rules = $rules;
        }

        if (!\is_array($this->rules) || !\count($this->rules)) {
            return;
        }

        foreach ($this->rules as $name => $rule) {
            if ($this->has($name)) {
                if (\is_array($this->rules_replace) && \count($this->rules_replace) > 0) {
                    foreach ($this->rules_replace as $var => $method) {
                        $rule = \str_replace($var, $this->{$method}(), $rule);
                    }
                }
                $this->fields[$name]->setOption('rules', $rule);

                // if has child & is type equal entity
                if (in_array($this->fields[$name]->getType(), ['entity'])) {
                    foreach ($this->fields[$name]->getChildren() as $child) {
                        $child->setOption('rules', $rule);
                    }
                }

            }
        }
    }

    /**
     * add submit button.
     *
     * @param array $options
     */
    protected function submitButton($options = [])
    {
        $this->add('submit', 'submit', \array_merge(['label' => trans('admin::common.save'), 'icon' => 'icon-rocket', 'wrapper' => [], 'attr' => ['class' => 'btn btn-success btn-anim']], $options));
    }

    /**
     * {@inheritdoc}
     */
    public function add($name, $type = 'text', array $options = [], $modify = false)
    {
        if (!isset($options['attr']['placeholder']) && isset($options['label'])) {
            $options['attr']['placeholder'] = $options['label'];
        }

        if ((isset($options['attr']['placeholder']) && !$options['attr']['placeholder']) || (isset($options['empty_value']) && $options['empty_value'] == false)) {
            unset($options['attr']['placeholder']);
        }

        $this->runIfMethodExists('beforeAddField', null, $name, $type, $options, $modify);

        return parent::add($name, $type, $options, $modify);
    }

    protected function ignoreUniqueId($rule, $value = null, $field = 'id')
    {
        return (null !== $value) ? $rule . ',' . $rule . ',' . $field : $rule;
    }

    protected function changeAllWrapper()
    {
        foreach ($this->getFields() as $field) {
            if (isset($field->getOption('wrapper')['skip'])) {
                $this->modify($field->getName(), $field->getType(), ['wrapper' => ['class' => $field->getOption('wrapper')['class'] . ' clearfix']]);
                continue;
            }
            $this->modify($field->getName(), $field->getType(), ['wrapper' => ['class' => $this->wrapper]]);
        }
    }

    protected function appendAddRemoveButton($class, $collaps = false, $options = [])
    {
        $options['attr'] = isset($options['attr']) ? $options['attr'] : [];
        $this->add('remove', 'button', [
            'label_show' => false,
            'attr' => \array_merge(['class' => 'btn btn-danger btn-outline remove-row', 'data-remove-collection' => $class], $options['attr']),
            'icon' => 'fa fa-minus',
        ]);

        $this->add('add', 'button', [
            'label_show' => false,
            'attr' => \array_merge(['class' => 'add-to-collection btn  btn-primary btn-outline add-row', 'data-collection' => $class, 'data-row-class' => '.form-group > .form-group.clearfix'], $options['attr']),
            'icon' => 'fa fa-plus',
        ]);

        if ($collaps) {
            $this->add('collapse', 'button', [
                'label_show' => false,
                'attr' => ['class' => 'btn btn-primary btn-outline collapse-row', 'data-target' => '#' . $this->getHtmlNameFormated('options'), 'data-toggle' => 'collapse'],
                'icon' => 'fa fa-angle-double-down',
            ]);
        }
    }

    protected function getHtmlNameFormated($extend = '')
    {
        return empty($extend) ? \trim($this->getHtmlName(), '_') : $this->transformToUnderScoreSyntax($this->name) . $extend;
    }

    protected function getHtmlName()
    {
        return $this->transformToUnderScoreSyntax($this->name);
    }

    /**
     * @param string $string
     *
     * @return string
     */
    public function transformToUnderScoreSyntax($string)
    {
        return \str_replace(['.', '[]', '[', ']'], '_', $string);
    }

    /**
     * encode id of entity data.
     *
     * @return \Closure
     */
    protected function encodeKeyEntityData(): \Closure
    {
        return function ($choices, $field) {
            return collect($choices)->mapWithKeys(function ($value, $key) {
                return [app('fakeid')->encode($key) => $value];
            })->toArray();
        };
    }


}
