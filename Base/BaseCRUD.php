<?php

/*
 * Copyright (c) 2017 Salah Alkhwlani <yemenifree@yandex.com>
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Modules\Admin\Base;

use Modules\Admin\Traits\Crud\HasCreate;
use Modules\Admin\Traits\Crud\HasCrud;
use Modules\Admin\Traits\Crud\HasDestroy;
use Modules\Admin\Traits\Crud\HasEdit;
use Modules\Admin\Traits\Crud\HasIndex;
use Modules\Admin\Traits\Crud\HasShow;

class BaseCRUD extends BaseController
{
    use HasCrud, HasIndex, HasCreate, HasShow, HasEdit, HasDestroy;
}
