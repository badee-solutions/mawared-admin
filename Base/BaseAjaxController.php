<?php

/*
 * Copyright (c) 2017 Salah Alkhwlani <yemenifree@yandex.com>
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Modules\Admin\Base;

use Ajax;
use Modules\Admin\Traits\Helper\HasRequest;

class BaseAjaxController extends BaseController
{
    use HasRequest;

    protected $json;
    protected $validate_as_alert = true;

    /**
     * @return \App\Services\Ajax\JsonResponse
     */
    protected function json()
    {
        return Ajax::setJson($this->json)->jsonResponse();
    }
}
