<?php

/*
 * Copyright (c) 2017 Salah Alkhwlani <yemenifree@yandex.com>
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Modules\Admin\Base;

use Illuminate\Contracts\Routing\UrlRoutable;
use Localization;
use McCool\LaravelAutoPresenter\BasePresenter as Presenter;

class BasePresenter extends Presenter implements UrlRoutable
{
    /**
     * Get value of wrappedObject.
     *
     * @param $key
     *
     * @return string
     */
    protected function getValue($key)
    {
        return $this->wrappedObject->$key;
    }

    /**
     * Get value of wrappedObject.
     *
     * @param $key
     *
     * @return string
     */
    protected function getTranslateValue($key)
    {
        return data_get($this->wrappedObject->$key, Localization::getCurrentLocale());
    }

    /**
     * Magic method access initially tries for local fields, then defers to the
     * decorated object.
     *
     * @param string $key
     *
     * @return mixed
     */
    public function __get(string $key)
    {
        if (\method_exists($this, $key)) {
            return $this->{$key}();
        }

        return $this->getValue($key);
    }
}
