<?php

/*
 * Copyright (c) 2017 Salah Alkhwlani <yemenifree@yandex.com>
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Modules\Admin\Base;

use Carbon\Carbon;
use Illuminate\Database\Query\Builder;
use Modules\Admin\Traits\DataTables\HasMinifiedAjax;
use Modules\Admin\Traits\Helper\HasBooTraits;
use Modules\Admin\Traits\Helper\HasQueryModel;
use Modules\Admin\Traits\Helper\HasRunIfExists;
use ReflectionClass;
use Yajra\DataTables\EloquentDataTable;
use Yajra\DataTables\Services\DataTable;

/**
 * Class BaseDataTables
 * @package Modules\Admin\Base
 * @method void beforeBuildColumns()
 * @method void afterBuildColumns()
 * @method void beforeGetAction($model)
 * @property string $module
 * @property string $link
 */
class BaseDataTables extends DataTable
{
    use HasBooTraits, HasQueryModel, HasRunIfExists, HasMinifiedAjax;

    protected $customFunctionsTableBuilder = [];
    protected $excelAccess = [];
    protected $contentType = null;
    protected $hasAction = false;
    protected $dates_format = [];
    protected $time_format = [];
    protected $hasButtons = true;
    protected $builderParmets;
    /** @var EloquentDataTable */
    protected $builder_table;
    protected $escapeColumns = [];
    protected $rawColumns = ['action'];
    protected $buttons = ['create',
        'reset',
        'reload',];
    protected $action = [];
    protected $dom = 'Bfrtip';
    /**
     * @var null|string
     */
    protected $transformer = null;

    public function __construct()
    {
        // boot traits
        $this->bootTraits();
    }

    /**
     * @param $repository
     */
    public function setRepository($repository)
    {
        $this->repository = $repository;
    }

    /**
     * Get Contact type for log.
     *
     * @return mixed|null
     */
    public function getContentTypeForLog()
    {
        return (null === $this->contentType) ? \str_replace('data table', '', snake_case((new ReflectionClass(static::class))->getShortName(), ' ')) : $this->contentType;
    }

    /**
     * Display ajax response.
     *
     * @param $source
     *
     * @return EloquentDataTable
     */
    public function dataTable($source = null)
    {
        if (is_null($source) && method_exists($this, 'query')) {
            $source = app()->call([$this, 'query']);
            $source = $this->applyScopes($source);
        }

        return $this->buildColumns($source);
    }

    /**
     * build Columns data with filter.
     *
     * @param $query
     *
     * @return EloquentDataTable
     */
    public function buildColumns($query)
    {
        $this->runIfMethodExists('beforeBuildColumns');

        $this->builder_table = datatables($query);
        
        if ($this->hasAction) {
            $this->builder_table->addColumn('action', function ($data) {
                return view('admin::datatable.actions', ['info' => $data, 'actions' => $this->resetAction()->getAction($data)]);
            });
        }

        $this->dateColumnFormat();

        $this->timeColumnFormat();

        // list of function to call before builder table return
        collect($this->customFunctionsTableBuilder)->each(function ($name) {
            $this->{$name}();
        });

        // add raw & escape columns
        $this->builder_table->rawColumns($this->rawColumns)->escapeColumns($this->escapeColumns);

        $this->runIfMethodExists('afterBuildColumns');

        return $this->builder_table;
    }

    /**
     * Get list of actions as array with url.
     *
     * @param $data
     *
     * @return array
     */
    public function getAction($data)
    {
        $this->runIfMethodExists('beforeGetAction', null, $data);

        return $this->action;
    }

    /**
     * set list actions.
     *
     * @param array $action
     *
     * @return BaseDataTables
     */
    public function setAction(array $action): BaseDataTables
    {
        $this->action = $action;

        return $this;
    }

    /**
     * clear action before render new row.
     *
     * @return $this
     */
    public function resetAction()
    {
        $this->action = [];

        return $this;
    }

    /**
     *  format date column.
     */
    protected function dateColumnFormat()
    {
        if (!\is_array($this->dates_format) || !\count($this->dates_format)) {
            return;
        }
        foreach ($this->dates_format as $dateColumn) {
            $this->builder_table->addColumn($dateColumn, function ($data) use ($dateColumn) {
                return isset($data->{$dateColumn}) ? $data->{$dateColumn}->format('Y-m-d') : trans('admin::common.n/a');
            });
        }
    }

    /**
     *  format time column.
     */
    protected function timeColumnFormat()
    {
        if (!\is_array($this->time_format) || !\count($this->time_format)) {
            return;
        }
        foreach ($this->time_format as $timeColumn) {
            $this->builder_table->addColumn($timeColumn, function ($data) use ($timeColumn) {
                return isset($data->{$timeColumn}) ? Carbon::createFromTimestamp(\strtotime($data->{$timeColumn}))->format('h:i A') : trans('admin::common.n/a');
            });
        }
    }

    /**
     * Get the query object to be processed by datatables.
     *
     * @return Builder|\Illuminate\Database\Eloquent\Builder
     */
    public function query()
    {
        return $this->getQuery();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     * @throws \Throwable
     */
    public function html()
    {
        $columns = $this->getColumns();

        if (isset($columns['id'])) {
            $columns['id']['width'] = '40px';
        }

        $this->minifiedAjax();
        $this->ajax['hasAjaxEditCrud'] = $this->hasAjaxEditCrud;
        $this->ajax['hasAjaxCreateCrud'] = $this->hasAjaxCreateCrud;

        $html = $this->builder()
            ->columns($columns)
            ->ajax($this->ajax)
            ->parameters(['oLanguage' => [
                'sUrl' => \App::isLocale('ar') ? asset('vendor/datatables/Arabic.json') : '',
                'oPaginate' => ['sFirst' => '»', 'sLast' => '«', 'sPrevious' => '«', 'sNext' => '»'],]])
            ->parameters([
                'dom' => $this->getDom(),
                'sFilterInput' => 'form-control serach-datatable',
                'buttons' => $this->hasButtons ? ['csv', 'excel', 'pdf', 'print'] : [],
                'initComplete' => view(config('admin.datatable.initComplete'))->render(),
            ])->parameters([
                'classes' => ['sFilterInput' => 'form-control serach-datatable'],
            ])->parameters([
                'responsive' => true,
                //'scrollX' => true,
                //'fixedColumns' => true,
            ])
            ->parameters($this->getBuilderParameters());

        if ($this->hasAction) {
            $html->addAction(['class' => 'all action-search', 'width' => '40px', 'orderable' => false, 'searchable' => false, 'title' => trans('admin::common.action')]);
        }

        return $html;
    }

    protected function getColumns()
    {
        return [];
    }

    /**
     * @return string
     */
    public function getDom(): string
    {
        return $this->dom;
    }

    /**
     * Get default builder parameters.
     *
     * @return array
     */
    protected function getBuilderParameters()
    {
        if (\is_array($this->builderParmets)) {
            return $this->builderParmets;
        }
        $builder = [
            'order' => [[0, 'desc']],
            'buttons' => $this->getButtons(),
        ];

        return $builder;
    }

    protected function getButtons()
    {
        return $this->hasButtons ? $this->buttons : [];
    }

    public function excel()
    {
        if (!\Sentinel::getUser()->hasAnyAccess(CanAdminAccess($this->excelAccess))) {
            abort(401);
        }

        //Activity::log(['contentId' => null, 'contentType' => $this->getContentTypeForLog(), 'action' => 'Export Excel']);
        parent::excel();
    }

    public function csv()
    {
        if (!\Sentinel::getUser()->hasAnyAccess(CanAdminAccess($this->excelAccess))) {
            abort(401);
        }

        //Activity::log(['contentId' => null, 'contentType' => $this->getContentTypeForLog(), 'action' => 'Export CVS']);
        parent::csv();
    }

    public function pdf()
    {
        if (!\Sentinel::getUser()->hasAnyAccess(CanAdminAccess($this->excelAccess))) {
            abort(401);
        }

        //Activity::log(['contentId' => null, 'contentType' => $this->getContentTypeForLog(), 'action' => 'Export PDF']);
        parent::pdf();
    }

    public function printPreview()
    {
        if (!\Sentinel::getUser()->hasAnyAccess(CanAdminAccess($this->excelAccess))) {
            abort(401);
        }

        //Activity::log(['contentId' => null, 'contentType' => $this->getContentTypeForLog(), 'action' => 'Print',]);
        return parent::printPreview();
    }

    public function setBuilderParameters($parmeters)
    {
        $this->builderParmets = $parmeters;

        return $this;
    }

    public function getColumnsForView()
    {
        return $this->getColumnsFromBuilder();
    }

    /**
     * @return EloquentDataTable
     */
    public function getBuilderTable(): EloquentDataTable
    {
        return $this->builder_table;
    }

    /**
     * get prefix route for curd links.
     *
     * @return mixed|null|string
     */
    public function getPrefixRoute()
    {
        return $this->link ?? $this->getPrefixModule();
    }

    /**
     * get prefix route for curd links.
     *
     * @return mixed|null|string
     */
    public function getPrefixModule()
    {
        return $this->module ?? $this->link ?? '';
    }
}
