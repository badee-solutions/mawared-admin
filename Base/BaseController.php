<?php

/*
 * Copyright (c) 2017 Salah Alkhwlani <yemenifree@yandex.com>
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Modules\Admin\Base;

use App\Traits\Admin\HasBreadcrumbs;
use Breadcrumbs;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller;
use Kris\LaravelFormBuilder\FormBuilderTrait;
use Modules\Admin\Traits\Requests\ValidatesRequests;

class BaseController extends Controller
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests, FormBuilderTrait, HasBreadcrumbs;

    protected $data = [];
    protected $validate_as_alert = false;

    protected function setTitle($title)
    {
        $this->addBreadcrumbItem($title);

        $this->data['title'] = $title;
        \SEO::setTitle($title);
        \SEO::setDescription($title);

        Breadcrumbs::register('home', function ($breadcrumbs) {
            $breadcrumbs->push(trans('admin::common.dashboard'), route('admin.dashboard'));
        });
    }
}
