<?php

/*
 * Copyright (c) 2017 Salah Alkhwlani <yemenifree@yandex.com>
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Modules\Admin\DataTables;

use Modules\Admin\Base\BaseDataTables;
use Modules\Admin\Traits\DataTables\HasActionHelper;
use Sentinel;

class RolesDataTable extends BaseDataTables
{
    use HasActionHelper;

    protected $hasAction = true;
    protected $dates_format = ['updated_at'];
    protected $link = 'roles';

    public function beforeGetAction($data)
    {
        $this->getDefaultCurdAction($data, true);
    }

    /**
     * Get the query object to be processed by datatables.
     *
     * @return \Illuminate\Database\Query\Builder|\Illuminate\Database\Eloquent\Builder
     */
    public function query()
    {
        return Sentinel::getRoleRepository()->withCount('users');
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'id' => ['title' => trans('common.#')],
            'name' => ['title' => trans('admin::roles.name')],
            'users_count' => ['title' => trans('admin::roles.users_count')],
        ];
    }
}
