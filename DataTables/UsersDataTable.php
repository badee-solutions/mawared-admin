<?php

/*
 * Copyright (c) 2017 Salah Alkhwlani <yemenifree@yandex.com>
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Modules\Admin\DataTables;

use App\User;
use Modules\Admin\Base\BaseDataTables;
use Modules\Admin\Traits\DataTables\HasActionHelper;
use Modules\Admin\Traits\DataTables\HasColumnHelper;

class UsersDataTable extends BaseDataTables
{
    use HasActionHelper, HasColumnHelper;

    protected $hasAction = true;
    protected $dates_format = ['updated_at'];
    protected $link = 'users';

    /**
     * @param User $data
     */
    protected function beforeGetAction($data)
    {
        $this->getDefaultCurdAction($data, true);
    }

    /**
     * Get the query object to be processed by datatables.
     *
     * @return \Illuminate\Database\Query\Builder|\Illuminate\Database\Eloquent\Builder
     */
    public function query()
    {
        return $this->getQuery()->whereHas('roles', function ($query) {
            return $query->where('is_admin', true);
        });
    }

    /**
     * @param User $data
     *
     * @return mixed
     */
    protected function editColumnActive($data)
    {
        return $data->getEnumsLabelText($data->active, 'active');
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'id' => ['title' => trans('common.#')],
            'name' => ['title' => trans('admin::users.name')],
            'email' => ['title' => trans('admin::users.email')],
            'mobile' => ['title' => trans('admin::users.mobile')],
//            'sex' => ['title' => trans('admin::users.sex')],
//            'active' => ['title' => trans('admin::users.active')],
        ];
    }
}
