<?php

/*
 * Copyright (c) 2017 Salah Alkhwlani <yemenifree@yandex.com>
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Modules\Admin\Entities;

use Cartalyst\Sentinel\Roles\EloquentRole;
use Illuminate\Database\Eloquent\SoftDeletes;
use Module;
use Modules\Admin\Traits\HasSelectDropList;
use Sofa\Eloquence\Eloquence;
use Sofa\Eloquence\Mutable;
use Spatie\Sluggable\HasSlug;
use Spatie\Sluggable\SlugOptions;

class Roles extends EloquentRole
{
    use HasSlug, HasSelectDropList, Eloquence,Mutable, SoftDeletes;

    /**
     * {@inheritdoc}
     */
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];
    //is_admin
    /**
     * {@inheritdoc}
     */
    protected $fillable = [
        'name',
        'slug',
        'permissions',
        'is_admin',
    ];

    protected $cascadeDeletes = ['users'];
    /**
     * Attributes setter mutators @ Eloquence\Mutable.
     *
     * @var array
     */
    protected $setterMutators = [
        'permissions' => self::class . '@fillArrayPermission',
        'is_admin' => 'falseifyEmpty',
    ];

    public static function buildPermission()
    {
        $permission = [];
        foreach (Module::all() as $module) {
            if (!\is_array($module->permission)) {
                continue;
            }
            //$lang_name = trans($module->alias.'::'.$module->lang_name);
            $permission[$module->name] = [];

            foreach ($module->permission as $name) {
                $permission[$module->name] = array_add($permission[$module->name], $name, self::getPermissionLang($module->alias, $name));
            }
        }

        return $permission;
    }

    protected static function getPermissionLang($module, $name)
    {
        return trans($module . '::permission.' . $name);
    }

    /**
     * @param self $query
     *
     * @return self
     */
    public function scopeAdmin($query)
    {
        return $query->where('is_admin', true);
    }

    /**
     * @param self $query
     *
     * @return self
     */
    public function scopeNotAdmin($query)
    {
        return $query->where('is_admin', false);
    }

    /**
     * Get the options for generating the slug.
     */
    public function getSlugOptions()
    {
        return SlugOptions::create()
            ->generateSlugsFrom('name')
            ->saveSlugsTo('slug')
            ->doNotGenerateSlugsOnUpdate();
    }

    public function scopeSlug($query, $slug)
    {
        return \is_array($slug) ? $query->whereIn('slug', $slug) : $query->where('slug', $slug);
    }

    /**
     * Fill true for permission before save to db.
     *
     * @param $permissions
     *
     * @return array
     */
    public function fillArrayPermission($permissions)
    {
        $permissions = \array_filter($permissions);

        return (\is_array($permissions) && \count($permissions)) ? \array_fill_keys($permissions, true) : [];
    }
}
