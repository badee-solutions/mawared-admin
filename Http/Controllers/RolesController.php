<?php

/*
 * Copyright (c) 2017 Salah Alkhwlani <yemenifree@yandex.com>
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Modules\Admin\Http\Controllers;

use Modules\Admin\Base\BaseCRUD;
use Modules\Admin\DataTables\RolesDataTable;
use Modules\Admin\Entities\Roles;
use Modules\Admin\Forms\RolesForm;

class RolesController extends BaseCRUD
{
    public function __construct(RolesDataTable $dataTable)
    {
        $this->middleware('permissions:roles');
        // init crud
        $this->setModel(Roles::class)
            ->setListTitle(trans('admin::roles.roles'))
            ->setUseForm(RolesForm::class)->setUrl('roles')
            ->setView('admin')
            ->setPrefixView('.base');

        // init database table.
        $this->dataTable = $dataTable;
        //$this->getDataTable()->setModel($this->getModel(false));
    }
}
