<?php

/*
 * Copyright (c) 2017 Salah Alkhwlani <yemenifree@yandex.com>
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Modules\Admin\Http\Controllers;

use App\Services\Ajax\Ajax;
use Assets;
use Cache;
use Eventy;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Kris\LaravelFormBuilder\FormBuilder;
use Kris\LaravelFormBuilder\FormBuilderTrait;
use Module;
use Modules\Admin\Base\BaseController;
use Modules\Admin\Traits\Helper\HasRunIfExists;
use Setting;
use URL;
use Breadcrumbs;

class SettingController extends BaseController
{
    use FormBuilderTrait, HasRunIfExists;

    /**
     * Url of setting page.
     */
    public $url = 'setting';
    public $view = 'admin';
    public $prefix_view = '.base';

    /**
     * Data of setting.
     *
     * @var string
     */
    protected $setting;
    protected $form;
    /**
     * Page title.
     *
     * @var string
     */
    protected $title;
    /**
     * kaye of setting in database.
     *
     * @var string
     */
    protected $key;
    /**
     * Module of setting.
     *
     * @var string
     */
    protected $module;
    protected $data = [];
    protected $items = 'items';

    /**
     * cache key they must clear after change setting.
     *
     * @var array
     */
    protected $cacheClear = [];
    /**
     * @var FormBuilder
     */
    protected $formBuilder;
    /**
     * @var Ajax
     */
    protected $ajax;
    protected $extendData = [];

    public function __construct(FormBuilder $formBuilder, Ajax $ajax)
    {
        $this->formBuilder = $formBuilder;
        $this->ajax = $ajax;
        $this->middleware('permissions:settings');
        $this->bootHasBreadcrumbs();
        $this->addBreadcrumbItem(trans('admin::common.setting'));
    }

    public function store($name = '')
    {
        //die(var_dump(request()->file('app.header_img')->store('public')));
        $this->getModule($name);

        Eventy::action('setting.before.request');

        $this->runIfMethodExists('actionBeforeForm', null);

        $form = $this->form($this->form, ['model' => $this->setting], $this->extendData);
        if (!$form->isValid()) {
            return $this->ajax->alert($form->getErrors(), 422)->jsonResponse(422);
        }

        $input = $form->getFieldValues();

        $input = Eventy::filter('setting.before.values', $input, $this->setting);

        $input = $this->runIfMethodExists('FilterInputBeforeSave', $input, $input);

        Eventy::action('setting.before.save', $input, $this->setting);

        Setting::set($input);

        Eventy::action('setting.after.save', $input, $this->setting);

        foreach ($this->cacheClear as $keycache) {
            Cache::forget($keycache);
        }

        return $this->ajax->setRedirect($this->url)->alert(trans('admin::setting.update'))->jsonResponse();
    }

    protected function getModule($name)
    {
        $module = Module::find($name);

        if (!$module || !\is_array($module->setting) || empty($module->setting['form']) || empty($module->setting['key'])) {
            throw (new ModelNotFoundException())->setModel(\get_class($module), [$name]);
        }
        $this->key = $module->setting['key'];
        $this->setting = $this->getSettings();
        $this->form = $module->setting['form'];
        $this->title = (!empty($module->setting['title'])) ? $module->setting['title'] : 'admin::common.setting';
        $this->module = $module;
        $this->url = URL::to(env('APP_ADMIN_PREFIX', 'admin') . '/setting/' . $this->module->name);
    }

    private function getSettings()
    {
        if (!isset($this->key)) {
            throw (new \Exception(trans('admin.error.key_setting_not_found'), 404));
        }

        return [$this->key => Setting::get($this->key)];
    }

    protected function prefixItems($key = null, $input = null)
    {
        $list_key = [$this->key, $this->items];
        if (null !== $key) {
            $list_key[] = $key;
        }
        if (null !== $input) {
            $list_key[] = $input;
        }

        return \implode('.', $list_key);
    }

    /**
     * Get List row table.
     *
     * @param $name
     *
     * @return mixed
     */
    protected function index($name = '')
    {
        $this->getModule($name);
        // todo implement permission check
        Assets::group('admin')->add('forms');
        Eventy::action('setting.before.form');
        $this->data['action'] = $this->url;
        $this->data['form'] = $this->formBuilder->create($this->form, ['enctype' => 'multipart/form-data', 'model' => $this->setting, 'method' => 'POST', 'class' => 'ajax form ui', 'url' => $this->data['action']], $this->extendData);
        $this->setTitle($this->data['form']->title);
        Eventy::action('setting.after.form');

        return view($this->view . '::admin' . $this->prefix_view . '.form', $this->data);
    }
}
