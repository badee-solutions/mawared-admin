<?php

/*
 * Copyright (c) 2017 Salah Alkhwlani <yemenifree@yandex.com>
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Modules\Admin\Http\Controllers;

use App\Services\Ajax\Ajax;
use Cartalyst\Sentinel\Checkpoints\NotActivatedException;
use Cartalyst\Sentinel\Checkpoints\ThrottlingException;
use Modules\Admin\Base\BaseController;
use Modules\Login\Http\Requests\LoginForm;
use Redirect;
use Sentinel;

class LoginController extends BaseController
{
    protected $validate_as_alert = true;

    public function login()
    {
        if (!Sentinel::guest()) {
            if (!Sentinel::hasAnyAccess(CanAdminAccess('access'))) {
                abort(401);
            } else {
                return redirect(Redirect::intended(env('APP_ADMIN_PREFIX', 'admin'))->getTargetUrl());
            }
        }

        $this->setTitle(trans('admin::user.control panel'));

        return view('admin::login');
    }

    /**
     * store a newly created resource in storage.
     *
     * @param  $request
     * @param Ajax $ajax
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(LoginForm $request, Ajax $ajax)
    {
        try {
            if (Sentinel::authenticateAndRemember($request->all())) {
                return $ajax->redirect($this->redirectWhenLoggedIn());
            }
            $error = trans('admin::user.error_login');
        } catch (NotActivatedException $e) {
            $error = trans('admin::user.error_wait_active');
        } catch (ThrottlingException $e) {
            $error = trans('error.your_ip_block');
        } catch (\Exception $e) {
            $error = $e->getmessage();
        }

        return $ajax->alert($error)->jsonResponse();
    }

    protected function redirectWhenLoggedIn()
    {
        if (!session()->has('url.intended')) {
            session(['url.intended' => url()->previous()]);
        }
        // Logged in successfully - redirect based on type of user
        if (Sentinel::hasAnyAccess(CanAdminAccess('access'))) {
            return Redirect::intended(env('APP_ADMIN_PREFIX', 'admin'))->getTargetUrl();
        }

        return Redirect::intended('/user/dashboard')->getTargetUrl();
    }

    /**
     * LogOut User.
     *
     * @return Redirect
     */
    public function logout()
    {
        Sentinel::logout(null, true);

        return redirect('/');
    }
}
