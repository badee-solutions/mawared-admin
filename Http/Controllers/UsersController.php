<?php

/*
 * Copyright (c) 2017 Salah Alkhwlani <yemenifree@yandex.com>
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Modules\Admin\Http\Controllers;

use App\User;
use Modules\Admin\Base\BaseCRUD;
use Modules\Admin\DataTables\UsersDataTable;
use Modules\Admin\Entities\Roles;
use Modules\Admin\Forms\AdminUserForm;
use Modules\Admin\Scope\UserRolesScope;

class UsersController extends BaseCRUD
{
    public function __construct(UsersDataTable $dataTable)
    {
        $this->middleware('permissions:admins');

        // init crud
        $this->setModel(User::class)
            ->setListTitle(trans('admin::users.users'))
            ->setUseForm(AdminUserForm::class)->setUrl('users')
            ->setView('admin')
            ->setPrefixView('.base');

        User::addGlobalScope(new UserRolesScope(Roles::admin()->get()->pluck('id')->toArray()));

        // init database table.
        $this->dataTable = $dataTable;
        $this->getDataTable()->setModel($this->getModel(false));
    }

    protected function filterUpdateInputBeforeFill($page, $input)
    {
        return $this->filterCreateBeforeSave($input);
    }

    protected function filterCreateBeforeSave($input)
    {
        if (empty($input['password'])) {
            unset($input['password']);
        }

        return $input;
    }

    protected function afterCreateSave($user, $input)
    {
        $this->afterUpdateSave($user, $input);

        if (!$user->active) {
            $user->active = true;
            $user->save();
        }
    }

    protected function afterUpdateSave($user, $input)
    {
        $this->setRoleToUser($user, $input['group']);
        if (request()->hasFile('cover')) {
            $user->clearMediaCollection('logo');
            $user->addMediaFromRequest('cover')->toMediaCollection('logo');
        }
    }

    protected function setRoleToUser($user, $role)
    {
        if (!$user->roles->contains($role)) {
            $user->roles()->sync([$role]);
        }
    }
}
