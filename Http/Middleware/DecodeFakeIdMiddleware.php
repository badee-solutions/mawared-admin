<?php

/*
 * Copyright (c) 2017 Salah Alkhwlani <yemenifree@yandex.com>
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Modules\Admin\Http\Middleware;

use Closure;
use Illuminate\Foundation\Http\Middleware\TransformsRequest;
use Modules\Admin\Traits\Requests\HasFakeIdsRequest;

class DecodeFakeIdMiddleware extends TransformsRequest
{
    use HasFakeIdsRequest;

    /**
     * @param \Illuminate\Http\Request $request
     * @param Closure $next
     * @param array ...$attributes
     *
     * @return mixed
     */
    public function handle($request, Closure $next, ...$attributes)
    {
        $this->attributes = $attributes;

        $request = $this->decodeFakeIdsInRoute($request);

        return parent::handle($request, $next, ...$attributes);
    }

    /**
     * Clean the request's data.
     *
     * @param \Illuminate\Http\Request $request
     */
    protected function clean($request)
    {
        $this->cleanParameterBag($request->query);

        // prevent duplicate transform if request same as query.
        if ($request->request === $request->query) {
            return;
        }

        if ($request->isJson()) {
            $this->cleanParameterBag($request->json());
        } else {
            $this->cleanParameterBag($request->request);
        }
    }

    /**
     * Clean the data in the given array.
     *
     * @param $parenKey
     * @param  array $data
     *
     * @return array
     */
    protected function transformArray($parenKey, array $data)
    {
        return collect($data)->map(function ($value, $key) use ($parenKey) {
            return $this->cleanValue($key, $value, $parenKey);
        })->all();
    }


    /**
     * Clean the given value.
     *
     * @param  string $key
     * @param  mixed $value
     *
     * @param null $parenKey
     *
     * @return mixed
     */
    protected function cleanValue($key, $value, $parenKey = null)
    {
        if (is_array($value)) {
            return $this->transformArray($key, $value);
        }

        return $this->transform($key, $value, $parenKey);
    }

    /**
     * Transform the given value.
     *
     * @param string $key
     * @param mixed $value
     *
     * @param null $parenKey
     *
     * @return mixed
     */
    protected function transform($key, $value, $parenKey = null)
    {
        if (\in_array($key, $this->attributes, true) || \in_array($parenKey, $this->attributes, true)) {
            return $this->decodeFakeId($value);
        }

        return $value;
    }
}
