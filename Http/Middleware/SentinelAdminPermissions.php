<?php

/*
 * Copyright (c) 2017 Salah Alkhwlani <yemenifree@yandex.com>
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Modules\Admin\Http\Middleware;

use Closure;
use Sentinel;

class SentinelAdminPermissions
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure                 $next
     * @param mixed                    $permission
     *
     * @return mixed
     */
    public function handle($request, Closure $next, $permission)
    {
        return app(SentinelAdminUser::class)->handle($request, function ($request) use ($next, $permission) {
            $permission = \explode('|', $permission);

            if (!Sentinel::hasAnyAccess(hasAdminAccess($permission))) {
                abort(403);
            }

            return $next($request);
        });
    }
}
