<?php

/*
 * Copyright (c) 2017 Salah Alkhwlani <yemenifree@yandex.com>
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Modules\Admin\Http\Middleware;

use Closure;

class SentinelApiRoleMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     *
     * @param $parameters
     *
     * @return mixed
     */
    public function handle($request, Closure $next, ...$parameters)
    {
        $role = \Sentinel::getUser()->roles->first();

        if (!$role || !in_array($role->getRoleSlug(), $parameters, true)) {
            return responder()->error(trans('admin::alert.you-do-not-have-access'))->respond(403);
        }

        return $next($request);
    }
}
