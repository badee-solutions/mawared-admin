<?php

/*
 * Copyright (c) 2017 Salah Alkhwlani <yemenifree@yandex.com>
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Modules\Admin\Http\Middleware;

use Closure;
use Modules\Login\Http\Middleware\Authenticate;
use Sentinel;
use URL;

class SentinelAdminUser
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure                 $next
     *
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        return app(Authenticate::class)->handle($request, function ($request) use ($next) {
            if (!Sentinel::hasAnyAccess(CanAdminAccess('access'))) {
                return redirect()->guest(URL::admin('login'));
            }

            return $next($request);
        });
    }
}
