<?php

/*
 * Copyright (c) 2017 Salah Alkhwlani <yemenifree@yandex.com>
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Modules\Admin\Http\Middleware;

use Closure;
use Sentinel;
use URL;

class SentinelRoleMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     *
     * @param $parameters
     *
     * @return mixed
     */
    public function handle($request, Closure $next, $parameters)
    {
        if (!Sentinel::inRole($parameters)) {
            return redirect()->guest(URL::admin('login'));
        }

        return $next($request);
    }
}
